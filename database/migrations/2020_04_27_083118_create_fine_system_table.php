<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFineSystemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fine_system', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('fine_id');
            $table->foreign('fine_id')->references('id')->on('fines')->onDelete('cascade');
            $table->string('days');
            $table->string('amount');
            $table->string('type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fine_system');
    }
}
