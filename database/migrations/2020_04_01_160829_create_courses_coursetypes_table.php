<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoursesCoursetypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses_coursetypes', function (Blueprint $table) {
                $table->unsignedInteger('course_id');
                $table->unsignedInteger('coursetype_id');
    
             //FOREIGN KEY CONSTRAINTS
               $table->foreign('course_id')->references('id')->on('courses')->onDelete('cascade');
               $table->foreign('coursetype_id')->references('id')->on('programtypes')->onDelete('cascade');
    
             //SETTING THE PRIMARY KEYS
               $table->primary(['course_id','coursetype_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses_coursetypes');
    }
}
