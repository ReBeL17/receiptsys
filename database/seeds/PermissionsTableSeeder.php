<?php

use App\Permission;
use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $permissions = [
            [
                
                'title' => 'user_management_access',
                'slug' => 'user-management-access',
            ],
            [
                
                'title' => 'permission_create',
                'slug' => 'permission-create',
            ],
            [
                
                'title' => 'permission_edit',
                'slug' => 'permission-edit',
            ],
            [
                
                'title' => 'permission_show',
                'slug' => 'permission-show',
            ],
            [
                
                'title' => 'permission_delete',
                'slug' => 'permission-delete',
            ],
            [
                
                'title' => 'permission_access',
                'slug' => 'permission-access',
            ],
            [
                
                'title' => 'role_create',
                'slug' => 'role-create',
            ],
            [
                
                'title' => 'role_edit',
                'slug' => 'role-edit',
            ],
            [
                
                'title' => 'role_show',
                'slug' => 'role-show',
            ],
            [
                
                'title' => 'role_delete',
                'slug' => 'role-delete',
            ],
            [
                
                'title' => 'group_access',
                'slug' => 'group-access',
            ],
            [
                
                'title' => 'group_create',
                'slug' => 'group-create',
            ],
            [
                
                'title' => 'group_edit',
                'slug' => 'group-edit',
            ],
            [
                
                'title' => 'group_show',
                'slug' => 'group-show',
            ],
            [
                
                'title' => 'group_delete',
                'slug' => 'group-delete',
            ],
            [
                
                'title' => 'group_access',
                'slug' => 'group-access',
            ],
            [
                
                'title' => 'user_create',
                'slug' => 'user-create',
            ],
            [
                
                'title' => 'user_edit',
                'slug' => 'user-edit',
            ],
            [
                
                'title' => 'user_show',
                'slug' => 'user-show',
            ],
            [
                
                'title' => 'user_delete',
                'slug' => 'user-delete',
            ],
            [
                
                'title' => 'user_access',
                'slug' => 'user-access',
            ],
            [
               
                'title' => 'faculty_create',
                'slug' => 'faculty-create',
            ],
            [
               
                'title' => 'faculty_edit',
                'slug' => 'faculty-edit',
            ],
            [
               
                'title' => 'faculty_show',
                'slug' => 'faculty-show',
            ],
            [
               
                'title' => 'faculty_delete',
                'slug' => 'faculty-delete',
            ],
            [
                
                'title' => 'faculty_access',
                'slug' => 'faculty-access',
            ],
            [
               
                'title' => 'program_create',
                'slug' => 'program-create',
            ],
            [
               
                'title' => 'program_edit',
                'slug' => 'program-edit',
            ],
            [
               
                'title' => 'program_show',
                'slug' => 'program-show',
            ],
            [
               
                'title' => 'program_delete',
                'slug' => 'program-delete',
            ],
            [
                
                'title' => 'program_access',
                'slug' => 'program-access',
            ],
            [
               
                'title' => 'programtype_create',
                'slug' => 'programtype-create',
            ],
            [
               
                'title' => 'programtype_edit',
                'slug' => 'programtype-edit',
            ],
            [
               
                'title' => 'programtype_show',
                'slug' => 'programtype-show',
            ],
            [
               
                'title' => 'programtype_delete',
                'slug' => 'programtype-delete',
            ],
            [
                
                'title' => 'programtype_access',
                'slug' => 'programtype-access',
            ],
            [
               
                'title' => 'course_create',
                'slug' => 'course-create',
            ],
            [
               
                'title' => 'course_edit',
                'slug' => 'course-edit',
            ],
            [
               
                'title' => 'course_show',
                'slug' => 'course-show',
            ],
            [
               
                'title' => 'course_delete',
                'slug' => 'course-delete',
            ],
            [
                
                'title' => 'course_access',
                'slug' => 'course-access',
            ],
            [
               
                'title' => 'batch_create',
                'slug' => 'batch-create',
            ],
            [
               
                'title' => 'batch_edit',
                'slug' => 'batch-edit',
            ],
            [
               
                'title' => 'batch_show',
                'slug' => 'batch-show',
            ],
            [
               
                'title' => 'batch_delete',
                'slug' => 'batch-delete',
            ],
            [
                
                'title' => 'batch_access',
                'slug' => 'batch-access',
            ],
             [
               
                'title' => 'student_create',
                'slug' => 'student-create',
            ],
            [
               
                'title' => 'student_edit',
                'slug' => 'student-edit',
            ],
            [
               
                'title' => 'student_show',
                'slug' => 'student-show',
            ],
            [
               
                'title' => 'student_delete',
                'slug' => 'student-delete',
            ],
            [
                
                'title' => 'student_access',
                'slug' => 'student-access',
            ],
            [
               
                'title' => 'fee_create',
                'slug' => 'fee-create',
            ],
            [
               
                'title' => 'fee_edit',
                'slug' => 'fee-edit',
            ],
            [
               
                'title' => 'fee_show',
                'slug' => 'fee-show',
            ],
            [
               
                'title' => 'fee_delete',
                'slug' => 'fee-delete',
            ],
            [
                
                'title' => 'fee_access',
                'slug' => 'fee-access',
            ],
            [
               
                'title' => 'fine_create',
                'slug' => 'fine-create',
            ],
            [
               
                'title' => 'fine_edit',
                'slug' => 'fine-edit',
            ],
            [
               
                'title' => 'fine_show',
                'slug' => 'fine-show',
            ],
            [
               
                'title' => 'fine_delete',
                'slug' => 'fine-delete',
            ],
            [
                
                'title' => 'fine_access',
                'slug' => 'fine-access',
            ],
            [
               
                'title' => 'payment_create',
                'slug' => 'payment-create',
            ],
            [
               
                'title' => 'payment_edit',
                'slug' => 'payment-edit',
            ],
            [
               
                'title' => 'payment_show',
                'slug' => 'payment-show',
            ],
            [
               
                'title' => 'payment_delete',
                'slug' => 'payment-delete',
            ],
            [
                
                'title' => 'payment_access',
                'slug' => 'payment-access',
            ],
        ];

        Permission::insert($permissions);
    }
}
