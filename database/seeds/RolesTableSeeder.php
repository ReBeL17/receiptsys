<?php

use Illuminate\Database\Seeder;
use App\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $roles = [
            [
                'id' => 1,
                'title' => 'Super Admin',
                'slug' => 'super-admin'
            ],
            [
                'id' => 2,
                'title' => 'Admin',
                'slug' => 'admin'
            ],
            [
                'id' => 3,
                'title' => 'System User',
                'slug' => 'system-user'
            ],
        ];

        Role::insert($roles);
    }
}
