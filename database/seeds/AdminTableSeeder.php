<?php

use Illuminate\Database\Seeder;
use App\Admin;

class AdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $admins = [
                    [
                        'id' => 1,
                        'name' => 'Super Admin',
                        'email' => 'superadmin@admin.com',
                        'password' => bcrypt('password'),
                        'remember_token' => null,
                    ],
                    [
                        'id' => 2,
                        'name' => 'Admin',
                        'email' => 'admin@admin.com',
                        'password' => bcrypt('password'),
                        'remember_token' => null,
                    ],
                    [
                        'id' => 3,
                        'name' => 'Arts',
                        'email' => 'arts@admin.com',
                        'password' => bcrypt('password'),
                        'remember_token' => null,
                    ],
                    [
                        'id' => 4,
                        'name' => 'Education',
                        'email' => 'education@admin.com',
                        'password' => bcrypt('password'),
                        'remember_token' => null,
                    ],
                ];

        Admin::insert($admins);
    }
}
