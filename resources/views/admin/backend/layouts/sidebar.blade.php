<!-- Sidebar -->
  <div class="sidebar">
    <!-- Sidebar user panel (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      <div class="image">
        <img src="{{ asset('/backend/dist/img/AdminLTELogo.png')}}" class="img-circle elevation-2" alt="User Image">
      </div>
      <div class="info">
        <a href="{{ route('admin.dashboard')}}" class="d-block">{{Auth::user()->name}}</a>
      </div>
    </div>

    
    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
             with font-awesome or any other icon font library -->
      
        <li class="nav-item">
          <a href="{{ route('admin.dashboard')}}" class="nav-link {{ (request()->is('admin')) ? 'active' : '' }}">
            <p>
            <i class="nav-icon fas fa-tachometer-alt"></i>
             <span> {{ trans('global.dashboard') }} </span>
            </p>
          </a>
        </li>
        
            @can('user-management-access')
            <li class="nav-item has-treeview {{ request()->is('admin/permissions*') ? 'menu-open' : '' }} {{ request()->is('admin/roles*') ? 'menu-open' : '' }} {{ request()->is('admin/users*') ? 'menu-open' : '' }}">
                <a class="nav-link nav-dropdown-toggle" href="#">
                    <i class="fa-fw fas fa-users">

                    </i>
                    <p>
                        <span>{{ trans('cruds.userManagement.title') }}</span>
                        <i class="right fa fa-fw fa-angle-left"></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    @can('permission-access')
                        <li class="nav-item">
                            <a href="{{ route("admin.permissions.index") }}" class="nav-link {{ request()->is('admin/permissions') || request()->is('admin/permissions/*') ? 'active' : '' }}">
                              &nbsp;&nbsp;&nbsp;<i class="fa-fw fas fa-unlock-alt">

                                </i>
                                <p>
                                    <span>{{ trans('cruds.permission.title') }}</span>
                                </p>
                            </a>
                        </li>
                    @endcan
                    @can('role-access')
                        <li class="nav-item">
                            <a href="{{ route("admin.roles.index") }}" class="nav-link {{ request()->is('admin/roles') || request()->is('admin/roles/*') ? 'active' : '' }}">
                              &nbsp;&nbsp;&nbsp;<i class="fa-fw fas fa-briefcase">

                                </i>
                                <p>
                                    <span>{{ trans('cruds.role.title') }}</span>
                                </p>
                            </a>
                        </li>
                    @endcan
                    @can('group-access')
                        <li class="nav-item">
                            <a href="{{ route("admin.groups.index") }}" class="nav-link {{ request()->is('admin/groups') || request()->is('admin/groups/*') ? 'active' : '' }}">
                              &nbsp;&nbsp;&nbsp;<i class="fas fa-user-friends"></i>
                                <p>
                                    <span>{{ trans('cruds.group.title') }}</span>
                                </p>
                            </a>
                        </li>
                    @endcan
                    @can('user-access')
                        <li class="nav-item">
                            <a href="{{ route("admin.users.index") }}" class="nav-link {{ request()->is('admin/users') || request()->is('admin/users/*') ? 'active' : '' }}">
                              &nbsp;&nbsp;&nbsp;<i class="fa-fw fas fa-user">

                                </i>
                                <p>
                                    <span>{{ trans('cruds.user.title') }}</span>
                                </p>
                            </a>
                        </li>
                    @endcan
                </ul>
            </li>
        @endcan
               
        @can('program-access')
        <li class="nav-item">
            <a href="{{ route("admin.programs.index") }}" class="nav-link {{ request()->is('admin/programs') || request()->is('admin/programs/*') ? 'active' : '' }}">
              <i class="fas fa-graduation-cap">

                </i>
                <p>
                    <span>{{ trans('cruds.program.title') }}</span>
                </p>
            </a>
        </li>
        @endcan

        @can('faculty-access')
        <li class="nav-item">
            <a href="{{ route("admin.faculty.index") }}" class="nav-link {{ request()->is('admin/faculty') || request()->is('admin/faculty/*') ? 'active' : '' }}">
              <i class="fas fa-university">

                </i>
                <p>
                    <span>{{ trans('cruds.faculty.title') }}</span>
                </p>
            </a>
        </li>
        @endcan

        @can('programtype-access')
        <li class="nav-item">
            <a href="{{ route("admin.programtypes.index") }}" class="nav-link {{ request()->is('admin/programtypes') || request()->is('admin/programtypes/*') ? 'active' : '' }}">
                <i class="fas fa-plus">

                </i>
                <p>
                    <span>{{ trans('cruds.programtype.title') }}</span>
                </p>
            </a>
        </li>
        @endcan

        @can('course-access')
        <li class="nav-item">
            <a href="{{ route("admin.courses.index") }}" class="nav-link {{ request()->is('admin/courses') || request()->is('admin/courses/*') ? 'active' : '' }}">
              <i class="fas fa-book">

                </i>
                <p>
                    <span>{{ trans('cruds.course.title') }}</span>
                </p>
            </a>
        </li>
        @endcan

        <li class="nav-item has-treeview {{ request()->is('admin/batches*') ? 'menu-open' : '' }} {{ request()->is('admin/students*') ? 'menu-open' : '' }}">
          <a class="nav-link nav-dropdown-toggle" href="#">
              <i class="fas fa-book-reader">

              </i>
              <p>
                  <span>{{ trans('cruds.studentManagement.title') }}</span>
                  <i class="right fa fa-fw fa-angle-left"></i>
              </p>
          </a>
          <ul class="nav nav-treeview">
            @can('batch-access')
            <li class="nav-item">
                <a href="{{ route("admin.batches.index") }}" class="nav-link {{ request()->is('admin/batches') || request()->is('admin/batches/*') ? 'active' : '' }}">
                  &nbsp;&nbsp;&nbsp;<i class="fas fa-tags">

                    </i>
                    <p>
                        <span>{{ trans('cruds.batch.title_singular') }}</span>
                    </p>
                </a>
            </li>
            @endcan

            @can('student-access')
            <li class="nav-item">
                <a href="{{ route("admin.students.index") }}" class="nav-link {{ request()->is('admin/students') || request()->is('admin/students/*') ? 'active' : '' }}">
                  &nbsp;&nbsp;&nbsp;<i class="fas fa-book-reader"></i>
                    <p>
                        <span>{{ trans('cruds.student.title') }}</span>
                    </p>
                </a>
            </li>
            @endcan
          </ul>
        </li>
        
        <li class="nav-item has-treeview {{ request()->is('admin/fees*') ? 'menu-open' : '' }} {{ request()->is('admin/fines*') ? 'menu-open' : '' }} {{ request()->is('admin/payments*') ? 'menu-open' : '' }}">
          <a class="nav-link nav-dropdown-toggle" href="#">
              <i class="far fa-money-bill-alt">

              </i>
              <p>
                  <span>{{ trans('cruds.feeManagement.title') }}</span>
                  <i class="right fa fa-fw fa-angle-left"></i>
              </p>
          </a>
          <ul class="nav nav-treeview">
              @can('fee-access')
              <li class="nav-item">
                  <a href="{{ route("admin.fees.index") }}" class="nav-link {{ request()->is('admin/fees') || request()->is('admin/fees/*') ? 'active' : '' }}">
                    &nbsp;&nbsp;&nbsp;<i class="far fa-money-bill-alt"></i>
                      <p>
                          <span>{{ trans('cruds.fee.title') }}</span>
                      </p>
                  </a>
              </li>
              @endcan

              @can('fine-access')
              <li class="nav-item">
                  <a href="{{ route("admin.fines.index") }}" class="nav-link {{ request()->is('admin/fines') || request()->is('admin/fines/*') ? 'active' : '' }}">
                    &nbsp;&nbsp;&nbsp;<i class="far fa-money-bill-alt"></i>
                      <p>
                          <span>{{ trans('cruds.fine.title') }}</span>
                      </p>
                  </a>
              </li>
              @endcan

              @can('payment-access')
              <li class="nav-item">
                  <a href="{{ route("admin.payments.index") }}" class="nav-link {{ request()->is('admin/payments') || request()->is('admin/payments/*') ? 'active' : '' }}">
                    &nbsp;&nbsp;&nbsp;<i class="far fa-money-bill-alt"></i>
                      <p>
                          <span>{{ trans('cruds.payment.title') }}</span>
                      </p>
                  </a>
              </li>
              @endcan
              
              @can('payment-access')
              <li class="nav-item">
                <a href="{{ route("admin.payments.allReceipts") }}" class="nav-link {{ request()->is('admin/receipts') || request()->is('admin/receipts/*') ? 'active' : '' }}">
                  &nbsp;&nbsp;&nbsp;<i class="far fa-money-bill-alt"></i>
                    <p>
                        <span>{{ trans('cruds.receipt.title') }}</span>
                    </p>
                </a>
            </li>
            @endcan
          </ul>
        </li>
      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>

  <div class="sidebar sidebar_footer" style="text-align:center;">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
      <li class="nav-item">
        <a href="{{route('admin.logout')}}" class="nav-link">
          <p>
          <i class="nav-icon fas fa-sign-out-alt"></i>
            <span>{{trans('global.logout')}}</span>
          </p>
        </a>
      </li>
    </ul>
  </div>
  <!-- /.sidebar -->