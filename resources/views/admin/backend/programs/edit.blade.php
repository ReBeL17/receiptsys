@extends('admin.backend.layouts.master')
@section('title','Edit Program')

@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.program.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.programs.update", [$program->slug]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="form-group">
                <label class="required" for="faculties">{{ trans('cruds.course.fields.faculty') }}</label>
                
                <select class="form-control {{ $errors->has('faculties') ? 'is-invalid' : '' }}" name="faculty_id" id="faculties" required>
                    @foreach($program->faculties as $faculty)
                    <option value="{{$faculty->id}}">{{$faculty->name}}</option>
                    @endforeach
                    @foreach($groups as $key => $group)
                        @if($group->title == 'Owner')
                            @foreach($faculties as $id => $faculty)
                                <option value="{{ $id }}" {{ in_array($id, old('faculty', [])) ? 'selected' : '' }}>{{ $faculty }}</option>
                            @endforeach
                            @break;

                        @elseif($group->title == 'Arts')
                            @foreach($faculties as $id => $faculty)
                                @if($faculty == 'Arts')
                                    <option value="{{ $id }}" {{ in_array($id, old('faculty', [])) ? 'selected' : '' }}>{{ $faculty }}</option>
                                @endif
                            @endforeach
                        
                        @elseif($group->title == 'Education')
                            @foreach($faculties as $id => $faculty)
                                @if($faculty == 'Education')
                                    <option value="{{ $id }}" {{ in_array($id, old('faculty', [])) ? 'selected' : '' }}>{{ $faculty }}</option>
                                @endif
                            @endforeach
                        @endif
                    @endforeach
                </select>
                @if($errors->has('faculty_id'))
                    <span class="text-danger">{{ $errors->first('faculty_id') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.course.fields.faculty_helper') }}</span>
            </div>

            <div class="form-group">
                <label class="required" for="name">{{ trans('cruds.program.fields.title') }}</label>
                <input class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" type="text" name="name" id="name" value="{{ old('name', $program->name) }}" required>
                @if($errors->has('name'))
                    <span class="text-danger">{{ $errors->first('name') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.program.fields.title_helper') }}</span>
            </div>
          
            <div class="form-group">
                <label class="required" for="description">{{ trans('cruds.program.fields.description') }}</label>
                
                <textarea class="form-control {{ $errors->has('description') ? 'is-invalid' : '' }}" name="description" 
                                        style="width: 100%; height: 300px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" id="editor">{{ $program->description }}</textarea>
                            
                @if($errors->has('description'))
                    <span class="text-danger">{{ $errors->first('description') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.program.fields.title_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-success" type="submit">
                    {{ trans('global.save') }}
                </button>
                <a href="{{route('admin.programs.index')}}" class="btn btn-danger">{{ trans('global.cancel') }}</a>
            </div>
        </form>
    </div>
</div>



@endsection