@extends('admin.backend.layouts.master')
@section('title','Collect Fees')

@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('cruds.payment.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.payments.update", [$payment->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            
            <div class="row">
                <div class="form-group col-md-6" >
                    <label class="col-md-3 required float-left" for="payment_no">{{ trans('cruds.payment.fields.payment_no') }} :</label>
                    <input class="form-control col-md-6  {{ $errors->has('payment_no') ? 'is-invalid' : '' }}" type="text" name="payment_no" id="payment_no" placeholder="payment no." required>
                    @if($errors->has('payment_no'))
                        <span class="text-danger">{{ $errors->first('payment_no') }}</span>
                    @endif
                    <span class="help-block">{{ trans('cruds.payment.fields.payment_no_helper') }}</span>
                </div>

                {{-- <div class="form-group col-md-6"></div> --}}
                
                <div class="form-group col-md-6">
                    <label class="col-md-4 required float-left" for="payment_date">{{ trans('cruds.payment.fields.payment_date') }} :</label>
                    <div class="input-group col-md-6">
                        <div class="input-group-prepend">
                          <span class="input-group-text">
                            <i class="far fa-calendar-alt"></i>
                          </span>
                        </div>
                    <input class="form-control float-right  {{ $errors->has('payment_date') ? 'is-invalid' : '' }}" type="text" name="payment_date" value="{{\Carbon\Carbon::now()->toDateString()}}" required readonly>
                      </div>
                      <!-- /.input group -->
                    
                    @if($errors->has('payment_date'))
                        <span class="text-danger">{{ $errors->first('payment_date') }}</span>
                    @endif
                    <span class="help-block">{{ trans('cruds.payment.fields.payment_date_helper') }}</span>
                </div>
            </div>

            <div class="row">
                <div class="form-group col-md-3">
                    <label class="col-md-6 required float-left" for="fee_id">{{ trans('cruds.payment.fields.fee_id') }} :</label>
                    <input class="form-control col-md-6 {{ $errors->has('fee_id') ? 'is-invalid' : '' }}" type="text" name="fee_id" id="fee_id" value="{{ $payment->fee->fee_no }}" placeholder="fee no." required readonly>
                        
                    @if($errors->has('fee_id'))
                        <span class="text-danger">{{ $errors->first('fee_id') }}</span>
                    @endif
                    <span class="help-block">{{ trans('cruds.payment.fields.fee_id_helper') }}</span>
                </div>
                <div class="col-md-1"></div>
                <div class="form-group col-md-5">
                    <label class="col-md-3 required float-left" for="due_date">{{ trans('cruds.payment.fields.due_date') }} :</label>
                    <div class="input-group col-md-6">
                        <div class="input-group-prepend">
                          <span class="input-group-text">
                            <i class="far fa-calendar-alt"></i>
                          </span>
                        </div>
                    <input class="form-control float-right  {{ $errors->has('due_date') ? 'is-invalid' : '' }}" type="text" value="{{$payment->fee->due_date}}" readonly>
                      </div>   
                    @if($errors->has('due_date'))
                        <span class="text-danger">{{ $errors->first('due_date') }}</span>
                    @endif
                    <span class="help-block">{{ trans('cruds.payment.fields.due_date_helper') }}</span>
                </div>
            </div>

            <div class="row">
                <div class="form-group col-md-4">
                    <label class="col-md-4 required float-left" for="regd_no">{{ trans('cruds.payment.fields.regd_no') }} :</label>
                <input class="form-control col-md-7 {{ $errors->has('regd_no') ? 'is-invalid' : '' }}" type="text" name="regd_no" id="regd_no" value="{{$payment->student->regd_no}}" required readonly>
                        
                    @if($errors->has('regd_no'))
                        <span class="text-danger">{{ $errors->first('regd_no') }}</span>
                    @endif
                    <span class="help-block">{{ trans('cruds.payment.fields.regd_no_helper') }}</span>
                </div>
                
                <div class="form-group col-md-4">
                    <label class="col-md-3 required float-left" for="name">{{ trans('cruds.payment.fields.name') }} :</label>
                <input class="form-control col-md-8 {{ $errors->has('name') ? 'is-invalid' : '' }}" type="text" name="name" id="name" value="{{$payment->student->name}}" required readonly>
                        
                    @if($errors->has('name'))
                        <span class="text-danger">{{ $errors->first('name') }}</span>
                    @endif
                    <span class="help-block">{{ trans('cruds.payment.fields.name_helper') }}</span>
                </div>

                <div class="form-group col-md-3">
                    <label class="col-md-4 required float-left" for="batch">{{ trans('cruds.payment.fields.batch') }} :</label>
                <input class="form-control col-md-8 {{ $errors->has('batch') ? 'is-invalid' : '' }}" type="text" name="batch" id="batch" value="{{$payment->student->batch->name}}" required readonly>
                        
                    @if($errors->has('batch'))
                        <span class="text-danger">{{ $errors->first('batch') }}</span>
                    @endif
                    <span class="help-block">{{ trans('cruds.payment.fields.batch_helper') }}</span>
                </div>
            </div>
            <div class="row form-group">
                <table class="table" style=" margin: 50px;" id="table">
                    <thead>
                        <tr>
                            <th>Particulars</th>
                            <th class="text-center">Unit</th>
                            <th class="text-center">Price</th>
                            <th class="text-center">Amount</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="col-md-5">{{$payment->fee->trimester}} fee</td>
                            <td class="col-md-2 text-center" id="unit">1</td>
                            <td class="col-md-2 text-center" id="price">{{$payment->due_amount}}</td>
                            <td class="col-md-2 text-center"><input type="text" name="fees" id="fees" value="{{$payment->due_amount}}" style="border:0px; text-align:center;" readonly></td>
                        </tr>
                        
                        @if($diff_days>15)
                            @if($fines)
                                @foreach($fines as $fine)
                                    @foreach($fine->fine_systems as $system)
                                        @if($system->days >= $diff_days)
                                            @if($system->type == 'per-day')
                                                @if(($diff_days-15)>0)
                                                    <tr>
                                                        <td class="col-md-5"><input type="text" name="fine_title[]" value="{{$fine->title}} ({{$fine->code}}) &nbsp;&nbsp;&nbsp;&nbsp; per day fine " style="border:0px; width:500px;" readonly></td>
                                                        <td class="col-md-2 text-center" id="unit"><input type="text" name="unit[]" value="{{$diff_days-15}} days" style="border:0px; text-align:center; width:90px;" readonly></td>
                                                        <td class="col-md-2 text-center" id="price"><input type="text" name="price[]" value="{{$system->amount}}" style="border:0px; text-align:center; width:90px;" readonly></td>
                                                        <td class="col-md-2 text-center"><input type="text" name="fine[]" id="amount{{$loop->index+1}}" value="{{($diff_days-15) * $system->amount}}" style="border:0px; text-align:center;" readonly></td>
                                                    </tr>
                                                @else 
                                                    <tr>
                                                        <td class="col-md-5"><input type="text" name="fine_title[]" value="{{$fine->title}} ({{$fine->code}}) &nbsp;&nbsp;&nbsp;&nbsp; per day fine " style="border:0px; width:500px;" readonly></td>
                                                        <td class="col-md-2 text-center" id="unit"><input type="text" name="unit[]" value="{{$diff_days}} days" style="border:0px; text-align:center; width:90px;" readonly></td>
                                                        <td class="col-md-2 text-center" id="price"><input type="text" name="price[]" value="{{$system->amount}}" style="border:0px; text-align:center; width:90px;" readonly></td>
                                                        <td class="col-md-2 text-center"><input type="text" name="fine[]" id="amount{{$loop->index+1}}" value="{{$diff_days * $system->amount}}" style="border:0px; text-align:center;" readonly></td>
                                                    </tr>
                                                @endif
                                            @else
                                                <tr>
                                                    <td class="col-md-5"><input type="text" name="fine_title[]" value="{{$fine->title}} ({{$fine->code}}) &nbsp;&nbsp;&nbsp;&nbsp; flat rate " style="border:0px; width:500px;" readonly></td>
                                                    <td class="col-md-2 text-center"><input type="text" name="unit[]" value="" style="border:0px; text-align:center; width:90px;" readonly></td>
                                                    <td class="col-md-2 text-center"><input type="text" name="price[]" value="" style="border:0px; text-align:center; width:90px;" readonly></td>
                                                    <td class="col-md-2 text-center"><input type="text" name="fine[]" id="amount{{$loop->index+1}}" value="{{$system->amount}}" style="border:0px; text-align:center;" readonly></td>
                                                </tr>
                                            @endif
                                            @break
                                        @endif

                                        @if($system->type == 'per-day')
                                            @if((strtolower($fine->faculty->name) == 'arts') && $diff_days>30)
                                                <tr>
                                                    <td class="col-md-5"><input type="text" name="fine_title[]" value="{{$fine->title}} ({{$fine->code}}) &nbsp;&nbsp;&nbsp;&nbsp; per day fine " style="border:0px; width:500px;" readonly></td>
                                                    <td class="col-md-2 text-center" id="unit"><input type="text" name="unit[]" value="{{$diff_days - $system->days}} days" style="border:0px; text-align:center; width:90px;" readonly></td>
                                                    <td class="col-md-2 text-center" id="price"><input type="text" name="price[]" value="{{$system->amount}}" style="border:0px; text-align:center; width:90px;" readonly></td>
                                                    <td class="col-md-2 text-center"><input type="text" name="fine[]" id="amount{{$loop->index+1}}" value="{{($diff_days - $system->days) * $system->amount}}" style="border:0px; text-align:center;" readonly></td>
                                                </tr>
                                            @else
                                                <tr>
                                                    <td class="col-md-5"><input type="text" name="fine_title[]" value="{{$fine->title}} ({{$fine->code}}) &nbsp;&nbsp;&nbsp;&nbsp; per day fine " style="border:0px; width:500px;" readonly></td>
                                                    <td class="col-md-2 text-center" id="unit"><input type="text" name="unit[]" value="{{$system->days}} days" style="border:0px; text-align:center; width:90px;" readonly></td>
                                                    <td class="col-md-2 text-center" id="price"><input type="text" name="price[]" value="{{$system->amount}}" style="border:0px; text-align:center; width:90px;" readonly></td>
                                                    <td class="col-md-2 text-center"><input type="text" name="fine[]" id="amount{{$loop->index+1}}" value="{{$system->days * $system->amount}}" style="border:0px; text-align:center;" readonly></td>
                                                </tr>
                                            @endif
                                        @else
                                            <tr>
                                                <td class="col-md-5"><input type="text" name="fine_title[]" value="{{$fine->title}} ({{$fine->code}}) &nbsp;&nbsp;&nbsp;&nbsp; flat rate " style="border:0px; width:500px;" readonly></td>
                                                <td class="col-md-2 text-center"><input type="text" name="unit[]" value="" style="border:0px; text-align:center; width:90px;" readonly></td>
                                                <td class="col-md-2 text-center"><input type="text" name="price[]" value="" style="border:0px; text-align:center; width:90px;" readonly></td>
                                                <td class="col-md-2 text-center"><input type="text" name="fine[]" id="amount{{$loop->index+1}}" value="{{$system->amount}}" style="border:0px; text-align:center;" readonly></td>
                                            </tr>
                                        @endif
                                    @endforeach
                                @endforeach
                            @endif
                        @endif
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="3" class="text-right"><b>Total:</b></td>
                            <td class="col-md-3 text-center"><input type="text" name="payment_amount" id="total"  style="border:0px; text-align:center;" readonly></td>
                        </tr>
                    </tfoot>
                </table>
            </div>

            <div class="form-group">
                <label class="required" for="description">{{ trans('cruds.payment.fields.description') }}</label>
                {{-- <input class="form-control {{ $errors->has('description') ? 'is-invalid' : '' }}" type="text" name="description" id="description" value="{{ old('description', '') }}" required> --}}
                <textarea class="form-control {{ $errors->has('description') ? 'is-invalid' : '' }}" name="description" 
                                        style="width: 100%; height: 300px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" id="editor"></textarea>
                            
                @if($errors->has('description'))
                    <span class="text-danger">{{ $errors->first('description') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.payment.fields.description_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-success" type="submit">
                    {{ trans('global.save') }}
                </button>
                <a href="{{route('admin.payments.index')}}" class="btn btn-danger">{{ trans('global.cancel') }}</a>
            </div>
        </form>
    </div>
</div>

@endsection


@section('scripts')

    <script>
        $(document).ready(function(){
           var fees = $('#fees').val();
           var amount1 = $('#amount1').val();
           var amount2 = $('#amount2').val();
           var add = fees;
           if(amount1 && amount2){
                var add = parseFloat(fees)+ parseFloat(amount1)+ parseFloat(amount2);
           }
           else if(amount1){
                var add = parseFloat(fees)+ parseFloat(amount1);
           }
           else if(amount2){
                var add = parseFloat(fees)+ parseFloat(amount2);            
           }
        //    alert(add);
        document.getElementById('total').value = add;
        });
        
    </script>
  
@endsection




