@extends('admin.backend.layouts.master')
@section('title','Collect Fees')
@section('styles')
<style>
    .disabled {
    pointer-events: none;
}
</style>
@endsection

@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('cruds.payment.title') }}
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable datatable-payment">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            {{ trans('cruds.payment.fields.id') }}
                        </th>
                        <th>
                            {{ trans('cruds.payment.fields.fee_id') }}
                        </th>
                        <th>
                            {{ trans('cruds.payment.fields.regd_no') }}
                        </th>
                        <th>
                            {{ trans('cruds.payment.fields.name') }}
                        </th>
                        <th>
                            {{ trans('cruds.payment.fields.payment_status') }}
                        </th>
                        <th>
                            {{ trans('cruds.payment.fields.fee_status') }}
                        </th>
                        <th>
                            {{ trans('cruds.payment.fields.amount') }}
                        </th>
                        <th>
                            &nbsp;
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($payments as $key => $payment)
                        <tr data-entry-id="{{ $payment->id }}">
                            <td>

                            </td>
                            <td>
                                {{ $loop->index + 1 ?? '' }}
                            </td>
                            <td>
                                {{ $payment->fee->fee_no ?? '' }}
                            </td>
                            <td>
                                {{ $payment->student->regd_no ?? '' }}
                            </td>
                            <td>
                                {{ $payment->student->name ?? '' }}
                            </td>
                            <td>
                                @if($payment->payment_status==0)
                                <span class="badge badge-info">unpaid</span>
                                @elseif($payment->payment_status==1)
                                <span class="badge badge-success">paid</span>
                                @elseif($payment->payment_status==2)
                                <span class="badge badge-danger">partially paid</span>
                                @endif
                            </td>
                            <td>
                                @if($payment->fee_status==0)
                                <span class="badge badge-info">due</span>
                                @elseif($payment->fee_status==1)
                                <span class="badge badge-success">cleared</span>
                                @elseif($payment->fee_status==2)
                                <span class="badge badge-danger">overdue</span>
                                @endif
            
                            </td>
                            <td>
                                {{ $payment->due_amount ?? '' }}
                            </td>
                            <td>
                                {{-- @can('payment-show')
                                    <a class="btn btn-xs btn-primary" href="{{ route('admin.payments.show', $payment->id) }}">
                                        {{ trans('global.view') }}
                                    </a>
                                @endcan --}}

                                @can('payment-edit')
                                  @if($payment->payment_status == 1 || $payment->fee_status == 1)
                                        <a type="button" class="btn btn-xs btn-default disabled">
                                            <i class="fa fa-plus-circle"> </i> Collect Fees
                                        </a>
                                        @else
                                        <a type="button" class="btn btn-xs btn-warning" href="{{ route('admin.payments.edit', $payment->id) }}" >
                                            <i class="fa fa-plus-circle"> </i> Collect Fees
                                        </a>
                                    @endif
                                @endcan

                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>


@endsection


@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
// @can('payment-delete')
//   let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
//   let deleteButton = {
//     text: deleteButtonTrans,
//     url: "{{ route('admin.payments.massDestroy') }}",
//     className: 'btn-danger',
//     action: function (e, dt, node, config) {
//       var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
//           return $(entry).data('entry-id')
//       });

//       if (ids.length === 0) {
//         alert('{{ trans('global.datatables.zero_selected') }}')

//         return
//       }

//       if (confirm('{{ trans('global.areYouSure') }}')) {
//         $.ajax({
//           headers: {'x-csrf-token': $('meta[name="csrf-token"]').attr('content')},
//           method: 'POST',
//           url: config.url,
//           data: { ids: ids, _method: 'DELETE' }})
//           .done(function () { location.reload() })
//       }
//     }
//   }
//   dtButtons.push(deleteButton)
// @endcan

  $.extend(true, $.fn.dataTable.defaults, {
    order: [[ 1, 'desc' ]],
    pageLength: 100,
  });
  $('.datatable-payment:not(.ajaxTable)').DataTable({ buttons: dtButtons })
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
        $($.fn.dataTable.tables(true)).DataTable()
            .columns.adjust();
    });
})

</script>
@endsection