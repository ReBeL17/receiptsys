@extends('admin.backend.layouts.master')
@section('title','View Fee')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.fee.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            
            <table class="table table-bordered table-striped">
                <tbody>
                    
                    <tr>
                        <th>
                            {{ trans('cruds.fee.fields.fee_no') }}
                        </th>
                        
                        <td>
                            {{ $fee->fee_no ?? '-' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.fee.fields.batch') }}
                        </th>
                       
                        <td>
                            <span class="badge badge-info">{{ $fee->batch->name ?? '-' }}</span>
                        </td>
                    </tr>                
                    <tr>
                        <th>
                            {{ trans('cruds.fee.fields.trimester') }}
                        </th>
                        
                        <td>
                            {{ $fee->trimester ?? '-' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.fee.fields.amount') }}
                        </th>
                        
                        <td>
                            {{$fee->amount ?? '-' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.fee.fields.billing_date') }}
                        </th>
                        <td>
                            {{$fee->billing_date ?? '-' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.fee.fields.due_date') }}
                        </th>
                        <td>
                            {{$fee->due_date ?? '-' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.fee.fields.description') }}
                        </th>
                        <td>
                            {{ html_entity_decode(strip_tags($fee->description)) ?? '-' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.fee.fields.created_by') }}
                        </th>
                        <td>
                            {{ $fee->created_by ?? '-' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.fee.fields.created_at') }}
                        </th>
                        <td>
                            {{ $fee->created_at ?? '-' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.fee.fields.updated_by') }}
                        </th>
                        <td>
                            {{ $fee->updated_by ?? '-' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.fee.fields.updated_at') }}
                        </th>
                        <td>
                            {{ $fee->updated_at ?? '-' }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.fees.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>



@endsection