@extends('admin.backend.layouts.master')
@section('title','View Student')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.student.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            
            <table class="table table-bordered table-striped">
                <tbody>
                    
                    <tr>
                        <th>
                            {{ trans('cruds.student.fields.regd_no') }}
                        </th>
                                                
                        <td>
                            {{ $student->regd_no ?? '-' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.student.fields.batch') }}
                        </th>
                        
                        <td>
                            <span class="badge badge-info">{{ $student->batch->name ?? '-' }}</span>
                        </td>
                    </tr>                
                    <tr>
                        <th>
                            {{ trans('cruds.student.fields.name') }}
                        </th>
                                                
                        <td>
                            {{ $student->name ?? '-' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.student.fields.address') }}
                        </th>
                                                
                        <td>
                            {{$student->address ?? '-' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.student.fields.contact') }}
                        </th>
                        <td>
                            {{$student->contact ?? '-' }}
                        </td>
                    </tr>
                    
                    <tr>
                        <th>
                            {{ trans('cruds.student.fields.created_at') }}
                        </th>
                        <td>
                            {{ $student->created_at ?? '-' }}
                        </td>
                    </tr>
                   
                    <tr>
                        <th>
                            {{ trans('cruds.student.fields.updated_at') }}
                        </th>
                        <td>
                            {{ $student->updated_at ?? '-' }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.students.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>



@endsection