@extends('admin.backend.layouts.master')
@section('title','Add Student')

@section('styles')
    <style>
        .multisteps-form__progress {
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(0, 1fr));
}

.multisteps-form__progress-btn {
  transition-property: all;
  transition-duration: 0.15s;
  transition-timing-function: linear;
  transition-delay: 0s;
  position: relative;
  padding-top: 20px;
  color: rgba(108, 117, 125, 0.7);
  text-indent: -9999px;
  border: none;
  background-color: transparent;
  outline: none !important;
  cursor: pointer;
}

@media (min-width: 500px) {
  .multisteps-form__progress-btn {
    text-indent: 0;
  }
}

.multisteps-form__progress-btn:before {
  position: absolute;
  top: 0;
  left: 50%;
  display: block;
  width: 13px;
  height: 13px;
  content: '';
  -webkit-transform: translateX(-50%);
          transform: translateX(-50%);
  transition: all 0.15s linear 0s, -webkit-transform 0.15s cubic-bezier(0.05, 1.09, 0.16, 1.4) 0s;
  transition: all 0.15s linear 0s, transform 0.15s cubic-bezier(0.05, 1.09, 0.16, 1.4) 0s;
  transition: all 0.15s linear 0s, transform 0.15s cubic-bezier(0.05, 1.09, 0.16, 1.4) 0s, -webkit-transform 0.15s cubic-bezier(0.05, 1.09, 0.16, 1.4) 0s;
  border: 2px solid currentColor;
  border-radius: 50%;
  background-color: #fff;
  box-sizing: border-box;
  z-index: 3;
}

.multisteps-form__progress-btn:after {
  position: absolute;
  top: 5px;
  left: calc(-50% - 13px / 2);
  transition-property: all;
  transition-duration: 0.15s;
  transition-timing-function: linear;
  transition-delay: 0s;
  display: block;
  width: 100%;
  height: 2px;
  content: '';
  background-color: currentColor;
  z-index: 1;
}

.multisteps-form__progress-btn:first-child:after {
  display: none;
}

.multisteps-form__progress-btn.js-active {
  color: #007bff;
}

.multisteps-form__progress-btn.js-active:before {
  -webkit-transform: translateX(-50%) scale(1.2);
          transform: translateX(-50%) scale(1.2);
  background-color: currentColor;
}

.multisteps-form__form {
  position: relative;
}

.multisteps-form__panel {
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 0;
  opacity: 0;
  visibility: hidden;
}

.multisteps-form__panel.js-active {
  height: auto;
  opacity: 1;
  visibility: visible;
}
    </style>
@endsection

@section('content')

<div class="multisteps-form">
    <!--progress bar-->
    <div class="row">
      <div class="col-12 col-lg-8 ml-auto mr-auto mb-4">
        <div class="multisteps-form__progress">
          <button class="multisteps-form__progress-btn js-active" type="button" title="Student Info">Student Info</button>
          <button class="multisteps-form__progress-btn" type="button" title="Academic Details">Academic Details</button>
        </div>
      </div>
    </div>
    <!--form panels-->
    <div class="row">
      <div class="col-12 col-lg-8 m-auto">
        <form class="multisteps-form__form" method="POST" action="{{ route("admin.students.store") }}" enctype="multipart/form-data">
            @csrf
          <!--single form panel-->
          <div class="multisteps-form__panel shadow p-4 rounded bg-white js-active" data-animation="scaleIn">
            <h3 class="multisteps-form__title">Student Details</h3>
            <div class="multisteps-form__content">
              
                <div class="form-group col-12  mt-4 mt-sm-0">
                    <input class="multisteps-form__input form-control {{ $errors->has('regd_no') ? 'is-invalid' : '' }}" type="text" name="regd_no" placeholder="{{ trans('cruds.student.fields.regd_no') }}" required/>
                    @if($errors->has('regd_no'))
                            <span class="text-danger">{{ $errors->first('regd_no') }}</span>
                        @endif
                        <span class="help-block">{{ trans('cruds.student.fields.regd_no_helper') }}</span>
                </div>

                <div class="form-group col-12 ">
                  <input class="multisteps-form__input form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" type="text" name="name" placeholder="{{ trans('cruds.student.fields.name') }}" required/>
                  @if($errors->has('name'))
                            <span class="text-danger">{{ $errors->first('name') }}</span>
                        @endif
                        <span class="help-block">{{ trans('cruds.student.fields.name_helper') }}</span>
                </div>
                              
                <div class="form-group col-12 ">
                  <input class="multisteps-form__input form-control {{ $errors->has('address') ? 'is-invalid' : '' }}" type="text" name="address" placeholder="{{ trans('cruds.student.fields.address') }}" required/>
                  @if($errors->has('address'))
                            <span class="text-danger">{{ $errors->first('address') }}</span>
                        @endif
                        <span class="help-block">{{ trans('cruds.student.fields.address_helper') }}</span>
                </div>
                <div class="form-group col-12  mt-4 mt-sm-0">
                  <input class="multisteps-form__input form-control {{ $errors->has('contact') ? 'is-invalid' : '' }}" type="text" name="contact" placeholder="{{ trans('cruds.student.fields.contact') }}" required/>
                  @if($errors->has('contact'))
                            <span class="text-danger">{{ $errors->first('contact') }}</span>
                        @endif
                        <span class="help-block">{{ trans('cruds.student.fields.contact_helper') }}</span>
                </div>
              
               
                <a href="{{route('admin.students.index')}}" class="btn btn-danger float-left">{{ trans('global.cancel') }}</a>
              <div class="button-row d-flex mt-4">
                <button class="btn btn-primary ml-auto js-btn-next" type="button" title="Next">Next</button>
              </div>
            </div>
          </div>
          <!--single form panel-->
          <div class="multisteps-form__panel shadow p-4 rounded bg-white" data-animation="scaleIn">
            <h3 class="multisteps-form__title">Academic Details</h3>
            <div class="multisteps-form__content">
                <div class="row">
                    <div class="form-group col-md-5">
                        <label class="required" for="faculty">{{ trans('cruds.student.fields.faculty') }}</label>
                        <select class="form-control {{ $errors->has('faculties') ? 'is-invalid' : '' }}" name="faculty_id" id="faculties" required>
                            @foreach($groups as $key => $group)
                            
                                @if($group->title == 'Owner')
                                    <option value="">Select a faculty...</option>
                                    @foreach($faculties as $id => $faculty)
                                        <option value="{{ $id }}" {{ in_array($id, old('faculty', [])) ? 'selected' : '' }}>{{ $faculty }}</option>
                                    @endforeach
                                    @break;
    
                                @elseif($group->title == 'Arts')
                                    @foreach($faculties as $id => $faculty)
                                        @if($faculty == 'Arts')
                                            <option value="{{ $id }}" {{ in_array($id, old('faculty', [])) ? 'selected' : '' }}>{{ $faculty }}</option>
                                        @endif
                                    @endforeach
                                
                                @elseif($group->title == 'Education')
                                    @foreach($faculties as $id => $faculty)
                                        @if($faculty == 'Education')
                                            <option value="{{ $id }}" {{ in_array($id, old('faculty', [])) ? 'selected' : '' }}>{{ $faculty }}</option>
                                        @endif
                                    @endforeach
                                @endif
                            @endforeach
                        </select>
                            
                        @if($errors->has('faculty_id'))
                            <span class="text-danger">{{ $errors->first('faculty_id') }}</span>
                        @endif
                        <span class="help-block">{{ trans('cruds.student.fields.faculty_helper') }}</span>
                    </div>
    
                    <div class="form-group col-md-1">
                    </div>
    
                    <div class="form-group col-md-5">
                        <label class="required" for="programs">{{ trans('cruds.student.fields.program') }}</label>
                        
                        <select class="form-control {{ $errors->has('programs') ? 'is-invalid' : '' }}" name="program_id" id="programs" required>
                            <option value="">Select a program...</option>
                        
                        </select>
                        @if($errors->has('program_id'))
                            <span class="text-danger">{{ $errors->first('program_id') }}</span>
                        @endif
                        <span class="help-block">{{ trans('cruds.student.fields.program_helper') }}</span>
                    </div>
                </div>
    
                <div class="row">
                    <div class="form-group col-md-5">
                        <label class="required" for="courses">{{ trans('cruds.student.fields.course') }}</label>
                        
                        <select class="form-control {{ $errors->has('course_id') ? 'is-invalid' : '' }}" name="course_id" id="courses" required>
                            <option value="">Select a course...</option>
                        
                        </select>
                        @if($errors->has('course_id'))
                            <span class="text-danger">{{ $errors->first('course_id') }}</span>
                        @endif
                        <span class="help-block">{{ trans('cruds.student.fields.course_helper') }}</span>
                    </div>
    
                    <div class="form-group col-md-1"></div>
    
                    <div class="form-group col-md-3">
                        <label class="required" for="batches">{{ trans('cruds.student.fields.batch') }}</label>
                        
                        <select class="form-control {{ $errors->has('batch_id') ? 'is-invalid' : '' }}" name="batch_id" id="batches" required>
                            <option value="">Select a batch...</option>
                        
                        </select>
                        @if($errors->has('batch_id'))
                            <span class="text-danger">{{ $errors->first('batch_id') }}</span>
                        @endif
                        <span class="help-block">{{ trans('cruds.student.fields.batch_helper') }}</span>
                    </div>
                </div>
              <div class="multisteps-form__form button-row d-flex mt-4">
                <button class="btn btn-primary js-btn-prev" type="button" title="Prev">Prev</button>
                <button class="btn btn-success ml-auto" type="submit" title="Save">{{ trans('global.save') }}</button>
              </div>
            </div>
          </div>
        </form>
       </div>
    </div>
  </div>
@endsection


@section('scripts')

    {{-- get programs of specific faculty --}}
    <script>
        //DOM elements
const DOMstrings = {
  stepsBtnClass: 'multisteps-form__progress-btn',
  stepsBtns: document.querySelectorAll(`.multisteps-form__progress-btn`),
  stepsBar: document.querySelector('.multisteps-form__progress'),
  stepsForm: document.querySelector('.multisteps-form__form'),
  stepsFormTextareas: document.querySelectorAll('.multisteps-form__textarea'),
  stepFormPanelClass: 'multisteps-form__panel',
  stepFormPanels: document.querySelectorAll('.multisteps-form__panel'),
  stepPrevBtnClass: 'js-btn-prev',
  stepNextBtnClass: 'js-btn-next' };


//remove class from a set of items
const removeClasses = (elemSet, className) => {

  elemSet.forEach(elem => {

    elem.classList.remove(className);

  });

};

//return exect parent node of the element
const findParent = (elem, parentClass) => {

  let currentNode = elem;

  while (!currentNode.classList.contains(parentClass)) {
    currentNode = currentNode.parentNode;
  }

  return currentNode;

};

//get active button step number
const getActiveStep = elem => {
  return Array.from(DOMstrings.stepsBtns).indexOf(elem);
};

//set all steps before clicked (and clicked too) to active
const setActiveStep = activeStepNum => {

  //remove active state from all the state
  removeClasses(DOMstrings.stepsBtns, 'js-active');

  //set picked items to active
  DOMstrings.stepsBtns.forEach((elem, index) => {

    if (index <= activeStepNum) {
      elem.classList.add('js-active');
    }

  });
};

//get active panel
const getActivePanel = () => {

  let activePanel;

  DOMstrings.stepFormPanels.forEach(elem => {

    if (elem.classList.contains('js-active')) {

      activePanel = elem;

    }

  });

  return activePanel;

};

//open active panel (and close unactive panels)
const setActivePanel = activePanelNum => {

  //remove active class from all the panels
  removeClasses(DOMstrings.stepFormPanels, 'js-active');

  //show active panel
  DOMstrings.stepFormPanels.forEach((elem, index) => {
    if (index === activePanelNum) {

      elem.classList.add('js-active');

      setFormHeight(elem);

    }
  });

};

//set form height equal to current panel height
const formHeight = activePanel => {

  const activePanelHeight = activePanel.offsetHeight;

  DOMstrings.stepsForm.style.height = `${activePanelHeight}px`;

};

const setFormHeight = () => {
  const activePanel = getActivePanel();

  formHeight(activePanel);
};

//STEPS BAR CLICK FUNCTION
DOMstrings.stepsBar.addEventListener('click', e => {

  //check if click target is a step button
  const eventTarget = e.target;

  if (!eventTarget.classList.contains(`${DOMstrings.stepsBtnClass}`)) {
    return;
  }

  //get active button step number
  const activeStep = getActiveStep(eventTarget);

  //set all steps before clicked (and clicked too) to active
  setActiveStep(activeStep);

  //open active panel
  setActivePanel(activeStep);
});

//PREV/NEXT BTNS CLICK
DOMstrings.stepsForm.addEventListener('click', e => {

  const eventTarget = e.target;

  //check if we clicked on `PREV` or NEXT` buttons
  if (!(eventTarget.classList.contains(`${DOMstrings.stepPrevBtnClass}`) || eventTarget.classList.contains(`${DOMstrings.stepNextBtnClass}`)))
  {
    return;
  }

  //find active panel
  const activePanel = findParent(eventTarget, `${DOMstrings.stepFormPanelClass}`);

  let activePanelNum = Array.from(DOMstrings.stepFormPanels).indexOf(activePanel);

  //set active step and active panel onclick
  if (eventTarget.classList.contains(`${DOMstrings.stepPrevBtnClass}`)) {
    activePanelNum--;

  } else {

    activePanelNum++;

  }

  setActiveStep(activePanelNum);
  setActivePanel(activePanelNum);

});

//SETTING PROPER FORM HEIGHT ONLOAD
window.addEventListener('load', setFormHeight, false);

//SETTING PROPER FORM HEIGHT ONRESIZE
window.addEventListener('resize', setFormHeight, false);


        $(document).ready(function(){
            var faculty_id = $("#faculties").val();

        // if faculty is already selected
            if(faculty_id != 0){
                $.ajax({
                    cache:false,
                    url: '{{ route('admin.programs.getspecificprograms') }}',
                    type: 'get',
                    data: {facultyId:faculty_id},
                    dataType: 'json',
                    beforeSend: function(request) {
                                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                                    },
                    success:function(data){
                        var len = data.length;
                        $("#programs").empty();
                        var programs = $("#programs").append("<option value=''>Select a program...</option>");
                        for( var i = 0; i<len; i++){
                            var id = data[i]['id'];
                            var name = data[i]['name'];
                            programs.append("<option value='"+id+"'>"+name+"</option>");
                        }
                    }
                });
            }

        // if changes is made on faculty selection
            $("#faculties").change(function(){
                var selected_id = $(this).val();
                $.ajax({
                    cache:false,
                    url: '{{ route('admin.programs.getspecificprograms') }}',
                    type: 'get',
                    data: {facultyId:selected_id},
                    dataType: 'json',
                    beforeSend: function(request) {
                                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                                    },
                    success:function(data){
                        var len = data.length;
                        $("#programs").empty();
                        
                        var programs = $("#programs").append("<option value=''>Select a program...</option>");
                        
                        for( var i = 0; i<len; i++){
                            var id = data[i]['id'];
                            var name = data[i]['name'];
                            programs.append("<option value='"+id+"'>"+name+"</option>");
                        }
                    }
                });
            });
       
        // if changes is made on program selection
        $("#programs").change(function(){
                var selected_id = $(this).val();
                var faculty_id = $('#faculties').val();
                // console.log(faculty_id)
                $.ajax({
                    cache:false,
                    url: '{{ route('admin.courses.getspecificcourses') }}',
                    type: 'get',
                    data: {
                            programId:selected_id,
                            facultyId:faculty_id
                            },
                    dataType: 'json',
                    beforeSend: function(request) {
                                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                                    },
                    success:function(data){
                        var len = data.length;
                        $("#courses").empty();
                        var courses = $("#courses").append("<option value=''>Select a course...</option>");
                        for( var i = 0; i<len; i++){
                            var id = data[i]['id'];
                            var name = data[i]['name'];
                            courses.append("<option value='"+id+"'>"+name+"</option>");
                        }
                    }
                });
            });

             // if changes is made on course selection
        $("#courses").change(function(){
                var selected_id = $(this).val();
                $.ajax({
                    cache:false,
                    url: '{{ route('admin.batches.getspecificbatches') }}',
                    type: 'get',
                    data: {
                            courseId:selected_id,
                            // facultyId:faculty_id
                            },
                    dataType: 'json',
                    beforeSend: function(request) {
                                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                                    },
                    success:function(data){
                        var len = data[0].length;
                        var len2 = data[1]['coursetypes'].length;
                        $("#batches").empty();
                        var batches = $("#batches").append("<option value=''>Select a batch...</option>");
                        for( var i = 0; i<len; i++){
                            var id = data[0][i]['id'];
                            var name = data[0][i]['name'];
                            batches.append("<option value='"+id+"'>"+name+"</option>");
                        }

                        $("#type").empty();
                        var type = $("#type").append("<option value=''>Select a type...</option>");
                        for( var i = 0; i<len2; i++){
                            var id = data[1]['coursetypes'][i]['id'];
                            var name = data[1]['coursetypes'][i]['name'];
                            type.append("<option value='"+id+"'>"+name+"</option>");
                        }

                    }
                });
            });

            
    }); 

    
    $(function () {
        $("#billing, #due").datepicker({  altFormat: "yy-mm-dd" }).val();
    })
    </script>
  
@endsection