@extends('admin.backend.layouts.master')
@section('title','Add Fine')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.fine.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.fines.store") }}" enctype="multipart/form-data">
            
            @csrf

            <div class="form-group">
                <label class="required" for="faculties">{{ trans('cruds.fine.fields.faculty') }}</label>
                
                <select class="form-control {{ $errors->has('faculties') ? 'is-invalid' : '' }}" name="faculty_id" id="faculties" required>
                    @foreach($groups as $key => $group)
                    
                        @if($group->title == 'Owner')
                            <option value="0">Select a faculty...</option>
                            @foreach($faculties as $id => $faculty)
                                <option value="{{ $id }}" {{ in_array($id, old('faculty', [])) ? 'selected' : '' }}>{{ $faculty }}</option>
                            @endforeach
                            @break;

                        @elseif($group->title == 'Arts')
                            @foreach($faculties as $id => $faculty)
                                @if($faculty == 'Arts')
                                    <option value="{{ $id }}" {{ in_array($id, old('faculty', [])) ? 'selected' : '' }}>{{ $faculty }}</option>
                                @endif
                            @endforeach
                        
                        @elseif($group->title == 'Education')
                            @foreach($faculties as $id => $faculty)
                                @if($faculty == 'Education')
                                    <option value="{{ $id }}" {{ in_array($id, old('faculty', [])) ? 'selected' : '' }}>{{ $faculty }}</option>
                                @endif
                            @endforeach
                        @endif
                    @endforeach
                </select>
                @if($errors->has('faculty_id'))
                    <span class="text-danger">{{ $errors->first('faculty_id') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.fine.fields.faculty_helper') }}</span>
            </div>

            <div class="form-group">
                <label class="required" for="title">{{ trans('cruds.fine.fields.title') }}</label>
                <input class="form-control {{ $errors->has('title') ? 'is-invalid' : '' }}" type="text" name="title" id="title" value="{{ old('title', '') }}" required>
                @if($errors->has('title'))
                    <span class="text-danger">{{ $errors->first('title') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.fine.fields.title_helper') }}</span>
                
            </div>

            <div class="form-group">
                <label class="required" for="code">{{ trans('cruds.fine.fields.code') }}</label>
                <input class="form-control {{ $errors->has('code') ? 'is-invalid' : '' }}" type="text" name="code" id="code" value="{{ old('code', '') }}">
                @if($errors->has('code'))
                    <span class="text-danger">{{ $errors->first('code') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.fine.fields.code_helper') }}</span>
                
            </div>

            <div class="form-group dynamic_field">
                <label class="required" for="fine">Fine System : </label>
                
                <div class="row" style="padding:5px;">
                    
                    <div class="col-md-1"></div>
                    <div class="col-md-3 ">
                        <label class="required" for="days">Days after due date</label>
                        <input class="form-control {{ $errors->has('days') ? 'is-invalid' : '' }}" type="number" name="days[]" id="days" value="{{ old('days', '') }}" required>
                        @if($errors->has('days'))
                            <span class="text-danger">{{ $errors->first('days') }}</span>
                        @endif
                    </div>

                    <div class="col-md-3">
                        <label class="required" for="amount">Amount</label>
                        <input class="form-control {{ $errors->has('amount') ? 'is-invalid' : '' }}" type="number" name="amount[]" id="amount" value="{{ old('amount', '') }}" required>
                        @if($errors->has('amount'))
                            <span class="text-danger">{{ $errors->first('amount') }}</span>
                        @endif
                    </div>

                    <div class="col-md-3">
                        <label class="required" for="type">{{ trans('cruds.fine.fields.type') }}</label>
                        <select class="form-control {{ $errors->has('type') ? 'is-invalid' : '' }}" name="type[]" id="type" value="{{ old('type', '') }}" required>
                            <option value="">--select fine type--</option>
                            <option value="flat-rate">flat rate</option>
                            <option value="per-day">per day</option>
                        </select>
                        @if($errors->has('type'))
                            <span class="text-danger">{{ $errors->first('type') }}</span>
                        @endif
                        
                    </div>
                    <div class="col-md-1">
                        <label class="required" for="action">Action</label>

                            <a class="btn btn-success btn-md" id="add" style="cursor:pointer;">
                                    <i class="fa fa-plus"></i>
                            </a>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="required" for="status">{{ trans('cruds.fine.fields.status') }} : </label>
                
                <div class="icheck-success d-inline {{ $errors->has('status') ? 'is-invalid' : '' }}">
                    <input type="checkbox" name="status" id="checkboxSuccess2" value="1">
                    <label for="checkboxSuccess2">
                      Active
                    </label>
                  </div>         
                @if($errors->has('status'))
                    <span class="text-danger">{{ $errors->first('status') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.fine.fields.status_helper') }}</span>
            </div>

            <div class="form-group">
                <label class="required" for="description">{{ trans('cruds.fine.fields.description') }}</label>
                
                <textarea class="form-control {{ $errors->has('description') ? 'is-invalid' : '' }}" name="description" 
                                        style="width: 100%; height: 300px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" id="editor">{{ old('description', '') }}</textarea>
                            
                @if($errors->has('description'))
                    <span class="text-danger">{{ $errors->first('description') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.fine.fields.title_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-success" type="submit">
                    {{ trans('global.save') }}
                </button>
                <a href="{{route('admin.fines.index')}}" class="btn btn-danger">{{ trans('global.cancel') }}</a>
            </div>
        </form>
    </div>
</div>

@endsection

@section('scripts')
    <script>
        $(document).ready(function(){
        //adding options dynamically
        var i = 1;
        $('#add').click(function(){
            i++;
            $('.dynamic_field').append('<div class="row" id="row'+i+'" style="padding:5px; margin-bottom:2px;"> <div class="col-md-1"></div> <div class="col-md-3"> <input class="form-control {{ $errors->has('days') ? 'is-invalid' : '' }}" type="number" name="days[]" id="days" value="{{ old('days', '') }}"> @if($errors->has('days')) <span class="text-danger">{{ $errors->first('days') }}</span> @endif </div> <div class="col-md-3"> <input class="form-control {{ $errors->has('amount') ? 'is-invalid' : '' }}" type="number" name="amount[]" id="amount" value="{{ old('amount', '') }}"> @if($errors->has('amount')) <span class="text-danger">{{ $errors->first('amount') }}</span> @endif </div> <div class="col-md-3"> <select class="form-control {{ $errors->has('type') ? 'is-invalid' : '' }}" name="type[]" id="type" value="{{ old('type', '') }}" required> <option value="">--select fine type--</option> <option value="flat-rate">flat rate</option> <option value="per-day">per day</option> </select> @if($errors->has('type')) <span class="text-danger">{{ $errors->first('type') }}</span> @endif </div> <div class="col-md-1"> <input type="button" id="'+i+'" class="btn btn-danger remove" value="X"> </div> </div>');                                                      
        });
        

        $('body').on('click','.remove',function(){
            var button_id = $(this).attr("id");
            $('#row'+button_id+'').remove();
        });
    }); 
    </script>
@endsection
