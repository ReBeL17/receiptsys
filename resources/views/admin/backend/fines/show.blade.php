@extends('admin.backend.layouts.master')
@section('title','View Fine')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.fine.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            
            <table class="table table-bordered table-striped">
                <tbody>
                   
                    <tr>
                        <th>
                            {{ trans('cruds.fine.fields.title') }}
                        </th>
                       
                        <td>
                            {{ $fine->title ?? '-' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.fine.fields.code') }}
                        </th>
                        
                        <td>
                            {{ $fine->code ?? '-' }}
                        </td>
                    </tr>
                     <tr>
                        <th>
                            {{ trans('cruds.fine.fields.faculty') }}
                        </th>
                        
                        <td>
                            {{ $fine->faculty->name ?? '-' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.fine.fields.status') }}
                        </th>
                        <td>
                            @if($fine->status==0)
                            <span class="badge badge-danger">Deactive</label>
                            @else
                            <span class="badge badge-success">Active</label>
                        @endif
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.fine.fields.days') }}
                        </th>
                        <td>
                            @foreach($fine_system as $fines)
                            {{ $fines->days ?? '-' }},
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.fine.fields.amount') }}
                        </th>
                        <td>
                            @foreach($fine_system as $fines)
                            {{ $fines->amount ?? '-' }},
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.fine.fields.type') }}
                        </th>
                        <td>
                            @foreach($fine_system as $fines)
                            {{ $fines->type ?? '-' }},
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.fine.fields.description') }}
                        </th>
                        <td>
                            {{ html_entity_decode(strip_tags($fine->description)) ?? '-' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.fine.fields.created_by') }}
                        </th>
                        <td>
                            {{ $fine->created_by ?? '-' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.fine.fields.created_at') }}
                        </th>
                        <td>
                            {{ $fine->created_at ?? '-' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.fine.fields.updated_by') }}
                        </th>
                        <td>
                            {{ $fine->updated_by ?? '-' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.fine.fields.updated_at') }}
                        </th>
                        <td>
                            {{ $fine->updated_at ?? '-' }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.fines.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>



@endsection