@extends('admin.backend.layouts.master')
@section('title','Fine')

@section('content')
@can('fine-create')
    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-primary" href="{{ route("admin.fines.create") }}">
                {{ trans('global.assign') }} {{ trans('cruds.fine.title_singular') }}
            </a>
        </div>
    </div>
@endcan
<div class="card">
    <div class="card-header">
        {{ trans('cruds.fine.title_singular') }} {{ trans('global.list') }}
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable datatable-fine">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            {{ trans('cruds.fine.fields.id') }}
                        </th>
                        <th>
                            {{ trans('cruds.fine.fields.title') }}
                        </th>
                        <th>
                            {{ trans('cruds.fine.fields.code') }}
                        </th>
                        <th>
                            {{ trans('cruds.fine.fields.faculty') }}
                        </th>
                        <th>
                            {{ trans('cruds.fine.fields.status') }}
                        </th>
                       
                        <th>
                            &nbsp;
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($fines as $key => $fine)
                        <tr data-entry-id="{{ $fine->id }}">
                            <td>

                            </td>
                            <td>
                                {{ $loop->index + 1 ?? '' }}
                            </td>
                            <td>
                                {{ $fine->title ?? '' }}
                            </td>
                            <td>
                                {{ $fine->code ?? '' }}
                            </td>
                            <td>
                                {{ $fine->faculty->name ?? '' }}
                            </td>
                            <td>
                                @if($fine->status==0)
                                    <span class="badge badge-danger">Deactive</label>
                                    @else
                                    <span class="badge badge-success">Active</label>
                                @endif
                            </td>
                            <td>
                                @can('fine-show')
                                    <a class="btn btn-xs btn-primary" href="{{ route('admin.fines.show', $fine->slug) }}">
                                        {{ trans('global.view') }}
                                    </a>
                                @endcan

                                @can('fine-edit')
                                    <a class="btn btn-xs btn-info" href="{{ route('admin.fines.edit', $fine->slug) }}">
                                        {{ trans('global.edit') }}
                                    </a>
                                @endcan

                                @can('fine-delete')
                                    <form action="{{ route('admin.fines.destroy', $fine->slug) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                    </form>
                                @endcan

                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection


@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('fine-delete')
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.fines.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': $('meta[name="csrf-token"]').attr('content')},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  dtButtons.push(deleteButton)
@endcan

  $.extend(true, $.fn.dataTable.defaults, {
    order: [[ 1, 'desc' ]],
    pageLength: 100,
  });
  $('.datatable-fine:not(.ajaxTable)').DataTable({ buttons: dtButtons })
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
        $($.fn.dataTable.tables(true)).DataTable()
            .columns.adjust();
    });
})

</script>
@endsection