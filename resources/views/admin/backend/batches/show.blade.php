@extends('admin.backend.layouts.master')
@section('title','View Batch')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.batch.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            
            <table class="table table-bordered table-striped">
                <tbody>
                    
                    <tr>
                        <th>
                            {{ trans('cruds.batch.fields.title') }}
                        </th>
                        <td>
                            {{ $batch->name }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.batch.fields.course') }}
                        </th>
                        <td>
                            <span class="badge badge-info">{{ $batch->course->name }}</span>
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.batch.fields.coursetype') }}
                        </th>
                        <td>
                            <span class="badge badge-info">{{ $batch->coursetype->name }}</span>
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.batch.fields.description') }}
                        </th>
                        <td>
                            {{ html_entity_decode(strip_tags($batch->description)) }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.batches.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>



@endsection