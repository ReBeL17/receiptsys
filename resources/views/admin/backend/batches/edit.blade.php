@extends('admin.backend.layouts.master')
@section('title','Edit Batch')

@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.batch.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.batches.update", [$batch->slug]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf

             <div class="form-group">
                <label class="required" for="faculties">{{ trans('cruds.batch.fields.faculty') }}</label>
                
                <select class="form-control {{ $errors->has('faculties') ? 'is-invalid' : '' }}" name="faculty_id" id="faculties" required>
                    <option value="{{$batch->course->faculty_id}}">{{$batch->course->faculty->name}}</option>
                    @foreach($groups as $key => $group)
                    
                        @if($group->title == 'Owner')
                            @foreach($faculties as $id => $faculty)
                                <option value="{{ $id }}" {{ in_array($id, old('faculty', [])) ? 'selected' : '' }}>{{ $faculty }}</option>
                            @endforeach
                            @break;

                        @elseif($group->title == 'Arts')
                            @foreach($faculties as $id => $faculty)
                                @if($faculty == 'Arts')
                                    <option value="{{ $id }}" {{ in_array($id, old('faculty', [])) ? 'selected' : '' }}>{{ $faculty }}</option>
                                @endif
                            @endforeach
                        
                        @elseif($group->title == 'Education')
                            @foreach($faculties as $id => $faculty)
                                @if($faculty == 'Education')
                                    <option value="{{ $id }}" {{ in_array($id, old('faculty', [])) ? 'selected' : '' }}>{{ $faculty }}</option>
                                @endif
                            @endforeach
                        @endif
                    @endforeach
                    
                </select>
                @if($errors->has('faculties'))
                    <span class="text-danger">{{ $errors->first('faculties') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.batch.fields.faculty_helper') }}</span>
            </div>

            <div class="form-group">
                <label class="required" for="programs">{{ trans('cruds.batch.fields.program') }}</label>
                
                <select class="form-control {{ $errors->has('program_id') ? 'is-invalid' : '' }}" name="program_id" id="programs" required>
                    <option value="{{$batch->course->program_id}}">{{$batch->course->program->name}}</option>
                    @foreach($programs as $id => $programs)
                        <option value="{{ $id }}">{{ $programs }}</option>
                    @endforeach                 
                </select>
                @if($errors->has('program_id'))
                    <span class="text-danger">{{ $errors->first('program_id') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.course.fields.program_helper') }}</span>
            </div>

            <div class="form-group">
                <label class="required" for="courses">{{ trans('cruds.batch.fields.course') }}</label>
                
                <select class="form-control {{ $errors->has('course_id') ? 'is-invalid' : '' }}" name="course_id" id="courses" required>
                    <option value="{{$batch->course_id}}">{{$batch->course->name}}</option>
                    @foreach($courses as $id => $courses)
                        <option value="{{ $id }}">{{ $courses }}</option>
                    @endforeach                 
                </select>
                @if($errors->has('course_id'))
                    <span class="text-danger">{{ $errors->first('course_id') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.batch.fields.course_helper') }}</span>
            </div>

            <div class="form-group">
                <label class="required" for="coursetypes">{{ trans('cruds.batch.fields.coursetype') }}</label>
                    <select class="form-control {{ $errors->has('coursetypes') ? 'is-invalid' : '' }}" name="coursetypes" id="types" required>                            
                    <option value='{{ $batch->coursetype_id }}'>{{ $batch->coursetype->name }}</option>                            
                    </select>   
                @if($errors->has('coursetypes'))
                    <span class="text-danger">{{ $errors->first('coursetypes') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.course.fields.coursetypes_helper') }}</span>
            </div>

            <div class="form-group">
                <label class="required" for="name">{{ trans('cruds.batch.fields.title') }}</label>
                <input class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" type="text" name="name" id="name" value="{{ old('name', $batch->name) }}" required>
                @if($errors->has('name'))
                    <span class="text-danger">{{ $errors->first('name') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.batch.fields.title_helper') }}</span>
            </div>
              
            <div class="form-group">
                <label class="required" for="description">{{ trans('cruds.batch.fields.description') }}</label>
                
                <textarea class="form-control {{ $errors->has('description') ? 'is-invalid' : '' }}" name="description" 
                                        style="width: 100%; height: 300px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" id="editor">{{ $batch->description }}</textarea>
                            
                @if($errors->has('description'))
                    <span class="text-danger">{{ $errors->first('description') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.batch.fields.title_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-success" type="submit">
                    {{ trans('global.save') }}
                </button>
                <a href="{{route('admin.batches.index')}}" class="btn btn-danger">{{ trans('global.cancel') }}</a>
            </div>
        </form>
    </div>
</div>

@endsection

@section('scripts')
<script>
    $(document).ready(function(){

        // if changes is made on faculty selection
            $("#faculties").change(function(){
                var selected_id = $(this).val();
                $.ajax({
                    cache:false,
                    url: '{{ route('admin.programs.getspecificprograms') }}',
                    type: 'get',
                    data: {facultyId:selected_id},
                    dataType: 'json',
                    beforeSend: function(request) {
                                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                                    },
                    success:function(data){
                        var len = data.length;
                        $("#programs").empty();
                        
                        var programs = $("#programs").append("<option value=''>Select a program...</option>");
                        
                        for( var i = 0; i<len; i++){
                            var id = data[i]['id'];
                            var name = data[i]['name'];
                            programs.append("<option value='"+id+"'>"+name+"</option>");
                        }
                    }
                });
            });
       
        // if changes is made on program selection
        $("#programs").change(function(){
                var selected_id = $(this).val();
                var faculty_id = $('#faculties').val();
                // console.log(faculty_id)
                $.ajax({
                    cache:false,
                    url: '{{ route('admin.courses.getspecificcourses') }}',
                    type: 'get',
                    data: {
                            programId:selected_id,
                            facultyId:faculty_id
                            },
                    dataType: 'json',
                    beforeSend: function(request) {
                                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                                    },
                    success:function(data){
                        var len = data.length;
                        $("#courses").empty();
                        var courses = $("#courses").append("<option value=''>Select a course...</option>");
                        for( var i = 0; i<len; i++){
                            var id = data[i]['id'];
                            var name = data[i]['name'];
                            courses.append("<option value='"+id+"'>"+name+"</option>");
                        }
                    }
                });
            });

            // if changes is made on course selection
        $("#courses").change(function(){
                var selected_id = $(this).val();
                $.ajax({
                    cache:false,
                    url: '{{ route('admin.courses.getcoursetypes') }}',
                    type: 'get',
                    data: {
                            courseId:selected_id
                            },
                    dataType: 'json',
                    beforeSend: function(request) {
                                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                                    },
                    success:function(data){
                        var len = data['coursetypes'].length;
                        // console.log(len);
                        $("#types").empty();
                        var types = $("#types").append("<option value=''>Select a course type...</option>");
                        for( var i = 0; i<len; i++){
                            var id = data['coursetypes'][i]['id'];
                            var name = data['coursetypes'][i]['name'];
                            types.append("<option value='"+id+"'>"+name+"</option>");
                        }
                    }
                });
            });
    }); 
</script>
@endsection