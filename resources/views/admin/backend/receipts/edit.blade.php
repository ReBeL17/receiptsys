@extends('admin.backend.layouts.master')
@section('title','Generate Receipt')

@section('content')

<div class="invoice p-3 mb-3">
    <!-- title row -->
    <div class="row">
      <div class="col-12">
        <h4>
          <i class="fas fa-globe"></i> egSkill.
          <small class="float-right">Date: {{\Carbon\Carbon::now()->toDateString()}}</small>
        </h4>
      </div>
      <!-- /.col -->
    </div>
    <hr>
    <div class="row invoice-info" style="margin: 10px; 0px; 10px; 0px;">
      <div class="col-sm-6 invoice-col">
      <strong>Receipt No. </strong>{{$receipt->payment_no}} <br>
      <strong>Payment Due:</strong> {{$receipt->payment_date}}<br>
      </div>
      <!-- /.col -->
    </div>
    <!-- info row -->
    <div class="row invoice-info">
      <div class="col-sm-6 invoice-col">
        <strong>Name : </strong>{{$receipt->student->name}} <br>
        <strong>Faculty : </strong>{{$receipt->student->batch->faculty->name}} <br>
      </div>
      <!-- /.col -->
      <div class="col-sm-6 invoice-col">
        <strong>Course : </strong>{{$receipt->student->batch->course->name}} <br>
        <strong>Batch : </strong>{{$receipt->student->batch->name}} <br>
      </div>
      <!-- /.col -->
     
    </div>
    <!-- /.row -->

    <!-- Table row -->
    <div class="row">
      <div class="col-12 table-responsive">
        <table class="table table-striped" style="margin-top:10px;">
          <thead>
              <tr>
                  <th>Particulars</th>
                  <th>Unit</th>
                  <th>Price</th>
                  <th>Amount</th>
              </tr>
          </thead>
          <tbody>
            <tr>
              <td>{{$receipt->fee->trimester}} fee</td>
              <td>1</td>
              <td>{{$receipt->fee->amount}}</td>
              <td>{{$receipt->fee->amount}}</td>
            </tr>
            @foreach($receipt->fine_payments as $fine)
            <tr>
              <td>{{$fine->fine_title}}</td>
              <td>{{$fine->unit}}</td>
              <td>{{$fine->price}}</td>
              <td>{{$fine->amount}}</td>
            </tr>
            @endforeach
          </tbody>
          <tfoot>
              <tr>
                  <td colspan="3" class="text-right"><b>Total:</b></td>
                  <td>{{$receipt->payment_amount}}</td>
              </tr>
          </tfoot>
      </table>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->

    <div class="row">
      <b>Deposit</b>
      {{-- <textarea class="form-control {{ $errors->has('description') ? 'is-invalid' : '' }}" name="description" 
        style="width: 100%; height: 300px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{ old('description', '') }}</textarea> --}}

    </div>
    
    <div class="row">
      <b>Narration</b>
    </div>

    <div class="row">
      <b>Description</b>  
    </div>

    <div class="row">
      <b>Authorized Signature</b>  
    </div>
    <!-- /.row -->

    <!-- this row will not appear when printing -->
    <div class="row no-print">
      <div class="col-12">
        <a href="{{route('admin.receipt-print', $receipt->slug)}}" target="_blank" class="btn btn-default float-right"><i class="fas fa-print"></i> Print</a>
        
      </div>
    </div>
  </div>
  <!-- /.invoice -->
@endsection





