<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Receipt | egSkill</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Bootstrap 4 -->

  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('/backend/plugins/fontawesome-free/css/all.min.css')}}">
  <!-- Ionicons -->
  {{-- <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css"> --}}
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('/backend/dist/css/adminlte.min.css')}}">

  <!-- Google Font: Source Sans Pro -->
  {{-- <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet"> --}}
</head>
<body>

<div class="invoice p-3 mb-3">
    <!-- title row -->
    <div class="row">
      <div class="col-12">
        <h4>
          <i class="fas fa-globe"></i> egSkill.
          <small class="float-right">Date: {{\Carbon\Carbon::now()->toDateString()}}</small>
        </h4>
      </div>
      <!-- /.col -->
    </div>
    <hr>
    <div class="row invoice-info" style="margin: 10px 0 10px;">
      <div class="col-sm-6 invoice-col">
      <strong>Receipt No. </strong>{{$receipt->payment_no}} <br>
      <strong>Payment Due:</strong> {{$receipt->payment_date}}<br>
      </div>
      <!-- /.col -->
    </div>
    <!-- info row -->
    <div class="row invoice-info">
      <div class="col-sm-6 invoice-col">
        <strong>Name : </strong>{{$receipt->student->name}} <br>
        <strong>Faculty : </strong>{{$receipt->student->batch->faculty->name}} <br>
      </div>
      <!-- /.col -->
      <div class="col-sm-6 invoice-col">
        <strong>Course : </strong>{{$receipt->student->batch->course->name}} <br>
        <strong>Batch : </strong>{{$receipt->student->batch->name}} <br>
      </div>
      <!-- /.col -->
     
    </div>
    <!-- /.row -->

    <!-- Table row -->
    <div class="row">
      <div class="col-12 table-responsive">
        <table class="table table-striped" style="margin-top:10px;">
          <thead>
              <tr>
                  <th>Particulars</th>
                  <th>Unit</th>
                  <th>Price</th>
                  <th>Amount</th>
              </tr>
          </thead>
          <tbody>
            <tr>
              <td>{{$receipt->fee->trimester}} fee</td>
              <td>1</td>
              <td>{{$receipt->fee->amount}}</td>
              <td>{{$receipt->fee->amount}}</td>
            </tr>
            @foreach($receipt->fine_payments as $fine)
            <tr>
              <td>{{$fine->fine_title}}</td>
              <td>{{$fine->unit}}</td>
              <td>{{$fine->price}}</td>
              <td>{{$fine->amount}}</td>
            </tr>
            @endforeach
          </tbody>
          <tfoot>
              <tr>
                  <td colspan="3" class="text-right"><b>Total:</b></td>
                  <td>{{$receipt->payment_amount}}</td>
              </tr>
          </tfoot>
      </table>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->

    <div class="row">
      <b>Deposit</b>
      {{-- <textarea class="form-control {{ $errors->has('description') ? 'is-invalid' : '' }}" name="description" 
        style="width: 100%; height: 300px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{ old('description', '') }}</textarea> --}}

    </div>
    
    <div class="row">
      <b>Narration</b>
    </div>

    <div class="row">
      <b>Description</b>  
    </div>

    <div class="row">
      <b>Authorized Signature</b>  
    </div>
    <!-- /.row -->

  </div>
  <!-- /.invoice -->
  <script type="text/javascript"> 
    window.addEventListener("load", window.print());
  </script>

</body>
</html>






