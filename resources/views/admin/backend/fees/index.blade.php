@extends('admin.backend.layouts.master')
@section('title','Fee')

@section('content')
@can('fee-create')
    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-primary" href="{{ route("admin.fees.create") }}">
                {{ trans('global.assign') }} {{ trans('cruds.fee.title_singular') }}
            </a>
        </div>
    </div>
@endcan
<div class="card">
    <div class="card-header">
        {{ trans('cruds.fee.title_singular') }} {{ trans('global.list') }}
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable datatable-fee">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            {{ trans('cruds.fee.fields.id') }}
                        </th>
                        <th>
                            {{ trans('cruds.fee.fields.fee_no') }}
                        </th>
                        <th>
                            {{ trans('cruds.fee.fields.batch') }}
                        </th>
                        <th>
                            {{ trans('cruds.fee.fields.trimester') }}
                        </th>
                        <th>
                            {{ trans('cruds.fee.fields.amount') }}
                        </th>
                        <th>
                            {{ trans('cruds.fee.fields.due_date') }}
                        </th>
                        <th>
                            &nbsp;
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($fees as $key => $fee)
                        <tr data-entry-id="{{ $fee->id }}">
                            <td>

                            </td>
                            <td>
                                {{ $loop->index + 1 ?? '' }}
                            </td>
                            <td>
                                {{ $fee->fee_no ?? '' }}
                            </td>
                            <td>
                                {{ $fee->batch->name ?? '' }}
                            </td>
                            <td>
                                {{ $fee->trimester ?? '' }}
                            </td>
                            <td>
                                {{ $fee->amount ?? '' }}
                            </td>
                            <td>
                                {{ $fee->due_date ?? '' }}
                            </td>
                            <td>
                                @can('fee-show')
                                    <a class="btn btn-xs btn-primary" href="{{ route('admin.fees.show', $fee->slug) }}">
                                        {{ trans('global.view') }}
                                    </a>
                                @endcan

                                @can('fee-edit')
                                    <a class="btn btn-xs btn-info" href="{{ route('admin.fees.edit', $fee->slug) }}">
                                        {{ trans('global.edit') }}
                                    </a>
                                @endcan

                                @can('fee-delete')
                                    <form action="{{ route('admin.fees.destroy', $fee->slug) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                    </form>
                                @endcan

                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection


@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('fee-delete')
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.fees.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': $('meta[name="csrf-token"]').attr('content')},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  dtButtons.push(deleteButton)
@endcan

  $.extend(true, $.fn.dataTable.defaults, {
    order: [[ 1, 'desc' ]],
    pageLength: 100,
  });
  $('.datatable-fee:not(.ajaxTable)').DataTable({ buttons: dtButtons })
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
        $($.fn.dataTable.tables(true)).DataTable()
            .columns.adjust();
    });
})

</script>
@endsection