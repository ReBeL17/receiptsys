@extends('admin.backend.layouts.master')
@section('title','Add Fee')

@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.fee.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.fees.store") }}" enctype="multipart/form-data">
            @csrf
            
            <div class="row">
                <div class="form-group col-md-6" >
                    <label class="col-md-3 required float-left" for="fee_no">{{ trans('cruds.fee.fields.fee_no') }} :</label>
                    <input class="form-control col-md-6  {{ $errors->has('fee_no') ? 'is-invalid' : '' }}" type="text" name="fee_no" id="fee_no" value="{{ old('fee_no', '') }}" placeholder="Fee no." required>
                    @if($errors->has('fee_no'))
                        <span class="text-danger">{{ $errors->first('fee_no') }}</span>
                    @endif
                    <span class="help-block">{{ trans('cruds.fee.fields.fee_no_helper') }}</span>
                </div>

                {{-- <div class="form-group col-md-6"></div> --}}
                
                <div class="form-group col-md-6">
                    <label class="col-md-4 required float-left" for="billing_date">{{ trans('cruds.fee.fields.billing_date') }} :</label>
                    <div class="input-group col-md-6">
                        <div class="input-group-prepend">
                          <span class="input-group-text">
                            <i class="far fa-calendar-alt"></i>
                          </span>
                        </div>
                        <input class="form-control float-right  {{ $errors->has('billing_date') ? 'is-invalid' : '' }}" type="text" name="billing_date" id="billing" data-date-format="yyyy-mm-dd" value="{{ old('billing_date', '') }}" required>
                      </div>
                      <!-- /.input group -->
                    
                    @if($errors->has('billing_date'))
                        <span class="text-danger">{{ $errors->first('billing_date') }}</span>
                    @endif
                    <span class="help-block">{{ trans('cruds.fee.fields.billing_date_helper') }}</span>
                </div>
            </div>

            <div class="row">
                <div class="form-group col-md-5">
                    <label class="required" for="faculty">{{ trans('cruds.fee.fields.faculty') }}</label>
                    <select class="form-control {{ $errors->has('faculties') ? 'is-invalid' : '' }}" name="faculty_id" id="faculties" required>
                        @foreach($groups as $key => $group)
                        
                            @if($group->title == 'Owner')
                                <option value="">Select a faculty...</option>
                                @foreach($faculties as $id => $faculty)
                                    <option value="{{ $id }}" {{ in_array($id, old('faculty', [])) ? 'selected' : '' }}>{{ $faculty }}</option>
                                @endforeach
                                @break;

                            @elseif($group->title == 'Arts')
                                @foreach($faculties as $id => $faculty)
                                    @if($faculty == 'Arts')
                                        <option value="{{ $id }}" {{ in_array($id, old('faculty', [])) ? 'selected' : '' }}>{{ $faculty }}</option>
                                    @endif
                                @endforeach
                            
                            @elseif($group->title == 'Education')
                                @foreach($faculties as $id => $faculty)
                                    @if($faculty == 'Education')
                                        <option value="{{ $id }}" {{ in_array($id, old('faculty', [])) ? 'selected' : '' }}>{{ $faculty }}</option>
                                    @endif
                                @endforeach
                            @endif
                        @endforeach
                    </select>
                        
                    @if($errors->has('faculty_id'))
                        <span class="text-danger">{{ $errors->first('faculty_id') }}</span>
                    @endif
                    <span class="help-block">{{ trans('cruds.fee.fields.faculty_helper') }}</span>
                </div>

                <div class="form-group col-md-1">
                </div>

                <div class="form-group col-md-5">
                    <label class="required" for="programs">{{ trans('cruds.fee.fields.program') }}</label>
                    
                    <select class="form-control {{ $errors->has('programs') ? 'is-invalid' : '' }}" name="program_id" id="programs" required>
                        <option value="">Select a program...</option>
                    
                    </select>
                    @if($errors->has('program_id'))
                        <span class="text-danger">{{ $errors->first('program_id') }}</span>
                    @endif
                    <span class="help-block">{{ trans('cruds.fee.fields.program_helper') }}</span>
                </div>
            </div>

            <div class="row">
                <div class="form-group col-md-5">
                    <label class="required" for="courses">{{ trans('cruds.fee.fields.course') }}</label>
                    
                    <select class="form-control {{ $errors->has('course_id') ? 'is-invalid' : '' }}" name="course_id" id="courses" required>
                        <option value="">Select a course...</option>
                    
                    </select>
                    @if($errors->has('course_id'))
                        <span class="text-danger">{{ $errors->first('course_id') }}</span>
                    @endif
                    <span class="help-block">{{ trans('cruds.fee.fields.course_helper') }}</span>
                </div>

                <div class="form-group col-md-1"></div>

                <div class="form-group col-md-3">
                    <label class="required" for="batches">{{ trans('cruds.fee.fields.batch') }}</label>
                    
                    <select class="form-control {{ $errors->has('batch_id') ? 'is-invalid' : '' }}" name="batch_id" id="batches" required>
                        <option value="">Select a batch...</option>
                    
                    </select>
                    @if($errors->has('batch_id'))
                        <span class="text-danger">{{ $errors->first('batch_id') }}</span>
                    @endif
                    <span class="help-block">{{ trans('cruds.fee.fields.batch_helper') }}</span>
                </div>

                <div class="form-group col-md-2">
                    <label class="required" for="type">{{ trans('cruds.fee.fields.type') }}</label>
                    
                    <select class="form-control {{ $errors->has('type') ? 'is-invalid' : '' }}" name="type" id="type" required>
                        <option value="0">Select a course type...</option>
                    
                    </select>
                    @if($errors->has('type'))
                        <span class="text-danger">{{ $errors->first('type') }}</span>
                    @endif
                    <span class="help-block">{{ trans('cruds.fee.fields.type_helper') }}</span>
                </div>
            </div>

            <div class="row">
                <div class="form-group col-md-4">
                    <label class="required" for="sem">{{ trans('cruds.fee.fields.grade') }}</label>
                    <select class="form-control {{ $errors->has('type') ? 'is-invalid' : '' }}" name="sem" id="sem" required>
                        <option value="0">Select a sem...</option>
                    
                    </select>
                    @if($errors->has('sem'))
                        <span class="text-danger">{{ $errors->first('sem') }}</span>
                    @endif
                    <span class="help-block">{{ trans('cruds.fee.fields.grade_helper') }}</span>
                </div>

                <div class="form-group col-md-1"></div>

                <div class="form-group col-md-3">
                    <label class="required" for="amount">{{ trans('cruds.fee.fields.amount') }}</label>
                    <input class="form-control {{ $errors->has('amount') ? 'is-invalid' : '' }}" type="number" name="amount" id="amount" value="{{ old('amount', '') }}" placeholder="Enter fee amount" required>
                    @if($errors->has('amount'))
                        <span class="text-danger">{{ $errors->first('amount') }}</span>
                    @endif
                    <span class="help-block">{{ trans('cruds.fee.fields.amount_helper') }}</span>
                </div>

                <div class="form-group col-md-3">
                    <label class="required" for="due_date">{{ trans('cruds.fee.fields.due_date') }}</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text">
                            <i class="far fa-calendar-alt"></i>
                          </span>
                        </div>
                        <input class="form-control float-right {{ $errors->has('due_date') ? 'is-invalid' : '' }}" type="text" name="due_date" id="due" data-date-format="yyyy-mm-dd" value="{{ old('due_date', '') }}">
                      </div>
                      <!-- /.input group -->
                      
                    @if($errors->has('due_date'))
                        <span class="text-danger">{{ $errors->first('due_date') }}</span>
                    @endif
                    <span class="help-block">{{ trans('cruds.fee.fields.due_date_helper') }}</span>
                </div>

            </div>

            <div class="form-group">
                <label class="required" for="description">{{ trans('cruds.fee.fields.description') }}</label>
                {{-- <input class="form-control {{ $errors->has('description') ? 'is-invalid' : '' }}" type="text" name="description" id="description" value="{{ old('description', '') }}" required> --}}
                <textarea class="form-control {{ $errors->has('description') ? 'is-invalid' : '' }}" name="description" 
                                        style="width: 100%; height: 300px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" id="editor">{{ old('description', '') }}</textarea>
                            
                @if($errors->has('description'))
                    <span class="text-danger">{{ $errors->first('description') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.fee.fields.description_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-success" type="submit">
                    {{ trans('global.save') }}
                </button>
                <a href="{{route('admin.fees.index')}}" class="btn btn-danger">{{ trans('global.cancel') }}</a>
            </div>
        </form>
    </div>
</div>

@endsection


@section('scripts')

    {{-- get programs of specific faculty --}}
    <script>
        $(document).ready(function(){
            var faculty_id = $("#faculties").val();

        // if faculty is already selected
            if(faculty_id != 0){
                $.ajax({
                    cache:false,
                    url: '{{ route('admin.programs.getspecificprograms') }}',
                    type: 'get',
                    data: {facultyId:faculty_id},
                    dataType: 'json',
                    beforeSend: function(request) {
                                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                                    },
                    success:function(data){
                        var len = data.length;
                        $("#programs").empty();
                        var programs = $("#programs").append("<option value=''>Select a program...</option>");
                        for( var i = 0; i<len; i++){
                            var id = data[i]['id'];
                            var name = data[i]['name'];
                            programs.append("<option value='"+id+"'>"+name+"</option>");
                        }
                    }
                });
            }

        // if changes is made on faculty selection
            $("#faculties").change(function(){
                var selected_id = $(this).val();
                $.ajax({
                    cache:false,
                    url: '{{ route('admin.programs.getspecificprograms') }}',
                    type: 'get',
                    data: {facultyId:selected_id},
                    dataType: 'json',
                    beforeSend: function(request) {
                                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                                    },
                    success:function(data){
                        var len = data.length;
                        $("#programs").empty();
                        
                        var programs = $("#programs").append("<option value=''>Select a program...</option>");
                        
                        for( var i = 0; i<len; i++){
                            var id = data[i]['id'];
                            var name = data[i]['name'];
                            programs.append("<option value='"+id+"'>"+name+"</option>");
                        }
                    }
                });
            });
       
        // if changes is made on program selection
        $("#programs").change(function(){
                var selected_id = $(this).val();
                var faculty_id = $('#faculties').val();
                // console.log(faculty_id)
                $.ajax({
                    cache:false,
                    url: '{{ route('admin.courses.getspecificcourses') }}',
                    type: 'get',
                    data: {
                            programId:selected_id,
                            facultyId:faculty_id
                            },
                    dataType: 'json',
                    beforeSend: function(request) {
                                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                                    },
                    success:function(data){
                        var len = data.length;
                        $("#courses").empty();
                        var courses = $("#courses").append("<option value=''>Select a course...</option>");
                        for( var i = 0; i<len; i++){
                            var id = data[i]['id'];
                            var name = data[i]['name'];
                            courses.append("<option value='"+id+"'>"+name+"</option>");
                        }
                    }
                });
            });

             // if changes is made on course selection
        $("#courses").change(function(){
                var selected_id = $(this).val();
                $.ajax({
                    cache:false,
                    url: '{{ route('admin.batches.getspecificbatches') }}',
                    type: 'get',
                    data: {
                            courseId:selected_id,
                            // facultyId:faculty_id
                            },
                    dataType: 'json',
                    beforeSend: function(request) {
                                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                                    },
                    success:function(data){
                        var len = data[0].length;
                        var len2 = data[1]['coursetypes'].length;
                        $("#batches").empty();
                        var batches = $("#batches").append("<option value=''>Select a batch...</option>");
                        for( var i = 0; i<len; i++){
                            var id = data[0][i]['id'];
                            var name = data[0][i]['name'];
                            batches.append("<option value='"+id+"'>"+name+"</option>");
                        }

                        $("#type").empty();
                        var type = $("#type").append("<option value=''>Select a type...</option>");
                        for( var i = 0; i<len2; i++){
                            var id = data[1]['coursetypes'][i]['id'];
                            var name = data[1]['coursetypes'][i]['name'];
                            type.append("<option value='"+id+"'>"+name+"</option>");
                        }

                    }
                });
            });

             // if changes is made on type selection
        $("#type").change(function(){
                var selected_id = $(this).val();
                var course_id = $('#courses').val();
                // console.log(course_id)
                $.ajax({
                    cache:false,
                    url: '{{ route('admin.fees.divideTypes') }}',
                    type: 'get',
                    data: {
                            typeId:selected_id,
                            courseId:course_id
                            },
                    dataType: 'json',
                    beforeSend: function(request) {
                                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                                    },
                    success:function(data){
                        var duration = data[0]['duration'];
                        var type = data[1]['name'];
                        console.log(duration);
                        console.log(type);
                        $("#sem").empty();
                        if(type.substr(0,8).toLowerCase()=='semester'){                            
                            var sem = $("#sem").append("<option value=''>Select a semester...</option>");
                            for( var i = 1; i<=duration*2; i++){
                                sem.append("<option value='"+i+" semester'>"+i+" semester</option>");
                            }                            
                        }
                        if(type.substr(0,9).toLowerCase()=='trimester'){   
                            var sem = $("#sem").append("<option value=''>Select a trimester...</option>");                         
                            for( var i = 1; i<=duration*4; i++){
                                sem.append("<option value='"+i+" trimester'>"+i+" trimester</option>");
                            }                            
                        }
                    }
                });
            });
    }); 

    
    $(function () {
        $("#billing, #due").datepicker({  altFormat: "yy-mm-dd" }).val();
    })
    </script>
  
@endsection