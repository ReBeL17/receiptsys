<?php

namespace App\Http\Controllers\Receipt;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Student;
use App\Models\Batch;
use App\Models\Faculty;
use App\Models\Program;
use App\Models\Course;
use Gate;
use Symfony\Component\HttpFoundation\Response;
use App\Http\Requests\Receipt\StoreStudentRequest;
use App\Http\Requests\Receipt\UpdateStudentRequest;
use App\Http\Requests\Receipt\MassDestroyStudentRequest;
use Session;
use Auth;

class StudentsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    //
    public function index()
    {
       
        abort_if(Gate::denies('student-access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $groups = Auth::user()->groups()->pluck('title');
        $students = [];
        
        //getting only accessible students for user group
        foreach($groups as $key => $group){
            if($group == 'Owner'){
                $students = Student::all();
                break;
            }
            else{
                $students_under_faculty[$key] = Student::whereHas('batch', function ($query) use ($group) {
                                                           $query->whereHas('course', function ($query) use ($group) {
                                                                $query->whereHas('faculty', function($q) use ($group) {
                                                                    $q->where('name', $group);
                                                                });
                                                            });
                                                        })->get();
            }
        }
        //converting multidimentional array of batches to single in order to return to view
        if(isset($students_under_faculty) && !empty($students_under_faculty)){
            foreach($students_under_faculty as $student){
                foreach($student as $s){
                    $students[] = $s;
                }
            }
        }
                // dd($students);
        return view('admin.backend.students.index', compact('students'));
    }

    public function create()
    {
        abort_if(Gate::denies('student-create'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $faculties = Faculty::all()->pluck('name','id');
        $groups = Auth::user()->groups;
        return view('admin.backend.students.create',compact('faculties','groups'));
    }

    public function store(StoreStudentRequest $request)
    {
        // dd($request->all());
        $data=[
            'name' => $request->name,
            'regd_no' => $request->regd_no,
            'slug' => str_slug($request->name),
            'batch_id' => $request->batch_id,
            'address' => $request->address,
            'contact' => $request->contact,
        ];
        // dd($data);
        $student = Student::create($data);

        Session::flash('flash_success', 'Student created successfully!.');
        Session::flash('flash_type', 'alert-success');
        return redirect()->route('admin.students.index');

    }

    public function edit(Student $student)
    {
        abort_if(Gate::denies('student-edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $faculties = Faculty::all()->pluck('name','id');
        $groups = Auth::user()->groups;
        $student->load('batch');
        $course = $student->batch->course;
        $program = $course->program;
        $faculty = $course->faculty;
        // dd($course->name);
        // $program_id = $fee->course->program_id;
        // dd($program_id);
        $programs = Program::whereHas('faculties' , function ($query) use ($faculty)
                                {
                                    $query->where('faculty_id', $faculty->id);
                                })
                                ->pluck('name','id');
        // dd($fee);
        $courses = Course::where([
                                    ['faculty_id',$faculty->id],
                                    ['program_id',$program->id]
                                ])->pluck('name','id');
        return view('admin.backend.students.edit', compact('student','faculties','groups','course','program','faculty','programs','courses'));
    }

    public function update(UpdateStudentRequest $request, Student $student)
    {
        // dd($request);
        $data=[
            'name' => $request->name,
            'regd_no' => $request->regd_no,
            'slug' => str_slug($request->name),
            'batch_id' => $request->batch_id,
            'address' => $request->address,
            'contact' => $request->contact,
        ];
        // dd($data);
        $student->update($data);

        Session::flash('flash_success', 'Student updated successfully!.');
        Session::flash('flash_type', 'alert-success');
        return redirect()->route('admin.students.index');

    }

    public function show(Student $student)
    {
        abort_if(Gate::denies('student-show'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $student->load('batch');
        return view('admin.backend.students.show', compact('student'));
    }

    public function destroy(Student $student)
    {
        abort_if(Gate::denies('student-delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $student->delete();

        Session::flash('flash_danger', 'Student has been deleted !.');
        Session::flash('flash_type', 'alert-danger');
        return back();

    }

    public function massDestroy(MassDestroyStudentRequest $request)
    {
        Student::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);

    }

    
}
