<?php

namespace App\Http\Controllers\Receipt;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ProgramType;
use Gate;
use Symfony\Component\HttpFoundation\Response;
use App\Http\Requests\Receipt\StoreProgramTypeRequest;
use App\Http\Requests\Receipt\UpdateProgramTypeRequest;
use App\Http\Requests\Receipt\MassDestroyProgramTypeRequest;
use Session;

class ProgramTypesController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    //
    public function index()
    {
       
        abort_if(Gate::denies('programtype-access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $programtypes = ProgramType::all();

        return view('admin.backend.programtypes.index', compact('programtypes'));
    }

    public function create()
    {
        abort_if(Gate::denies('programtype-create'), Response::HTTP_FORBIDDEN, '403 Forbidden');       

        return view('admin.backend.programtypes.create');
    }

    public function store(StoreProgramTypeRequest $request)
    {
        // dd($request->all());
        $data=[
            'name' => $request->name,
            'description' => $request->description,
            'slug' => str_slug($request->name)
        ];
        // dd($data);
        $programtype = ProgramType::create($data);

        Session::flash('flash_success', 'program type created successfully!.');
        Session::flash('flash_type', 'alert-success');
        return redirect()->route('admin.programtypes.index');

    }

    public function edit(ProgramType $programtype)
    {
        abort_if(Gate::denies('programtype-edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.backend.programtypes.edit', compact('programtype'));
    }

    public function update(UpdateProgramTypeRequest $request, ProgramType $programtype)
    {
        // dd($request);
        $data=[
            'name' => $request->name,
            'description' => $request->description,
            'slug' => str_slug($request->name)
        ];
        // dd($data);
        $programtype->update($data);

        Session::flash('flash_success', 'program type updated successfully!.');
        Session::flash('flash_type', 'alert-success');
        return redirect()->route('admin.programtypes.index');

    }

    public function show(ProgramType $programtype)
    {
        abort_if(Gate::denies('programtype-show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.backend.programtypes.show', compact('programtype'));
    }

    public function destroy(ProgramType $programtype)
    {
        abort_if(Gate::denies('programtype-delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $programtype->delete();

        Session::flash('flash_danger', 'program type has been deleted !.');
        Session::flash('flash_type', 'alert-danger');
        return back();

    }

    public function massDestroy(MassDestroyProgramTypeRequest $request)
    {
        ProgramType::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);

    }

    public function addType(Request $request)
    {
        $data=[
            'name' => $request->data['name'],
            'description' => $request->data['description'],
            'slug' => str_slug($request->data['name'])
        ];
        // dd($data);
        $programtype = ProgramType::create($data);
        return $programtype;
    }
}
