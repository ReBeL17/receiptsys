<?php

namespace App\Http\Controllers\Receipt;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Fee;
use App\Models\Payment;
use App\Models\Fee_Student;
use App\Models\Fine_Payment;
use App\Models\Fine;
use Gate;
use Symfony\Component\HttpFoundation\Response;
use App\Http\Requests\Receipt\StorePaymentRequest;
use App\Http\Requests\Receipt\UpdatePaymentRequest;
use App\Http\Requests\Receipt\MassDestroyPaymentRequest;
use Session;
use Auth;
use \Carbon\Carbon;

class PaymentsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    //
    public function index()
    {
       
        abort_if(Gate::denies('payment-access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $user = Auth::user();
        // $roles = $user->roles()->pluck('title');
        $groups = $user->groups()->pluck('title');
        $payments = [];
        // foreach($roles as $role) {
            
        //     if(strtolower($role) == 'super admin' || strtolower($role) == 'admin') {
        //         $payments =  Fee_Student::with('fee','student')->get();
        //     }
        //     else{
        //         $payments =  Fee_Student::with('fee','student')->where('created_by',$user->name)->get();
        //     }
        // }
    
        //getting only accessible fees for user group
            foreach($groups as $key => $group){
                if($group == 'Owner'){
                    $payments = Fee_Student::with('fee','student')->get();
                    break;
                }
                else{
                    $dues_under_faculty[$key] = Fee_Student::whereHas('fee', function ($query) use ($group) {
                                                            $query->whereHas('batch', function($query) use ($group) {
                                                                $query->whereHas('course', function($query) use ($group) {
                                                                    $query->whereHas('faculty', function($query) use ($group) {
                                                                        $query->where('name', $group);
                                                                    });
                                                                });
                                                            });
                                                        })->get();
                }
            }
        // converting multidimentional array of batches to single in order to return to view
            if(isset($dues_under_faculty) && !empty($dues_under_faculty)){
                foreach($dues_under_faculty as $dues){
                    foreach($dues as $due){
                        $payments[] = $due;
                    }
                }
            }
                        // dd($payments);

                
        return view('admin.backend.payments.index', compact('payments'));
    }

    public function create()
    {
        // abort_if(Gate::denies('payment-create'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        // $faculties = Faculty::pluck('name','id');
        // $groups = Auth::user()->groups;
        // return view('admin.backend.payments.create',compact('faculties','groups'));
    }

    public function store(StoreFineRequest $request)
    {
        // dd($request->all());
        // $data=[
        //     'title' => $request->title,
        //     'slug' => str_slug($request->title),
        //     'code' => $request->code,
        //     'faculty_id' => $request->faculty_id, 
        //     'status' => $request->status,
        //     'description' => $request->description,
        //     'created_by' => Auth::user()->name,
        // ];
        // $fine = Fine::create($data);

        // for($i=0;$i<count($request->amount);$i++){
        //     $data2=[
        //         'amount' => $request->amount[$i],
        //         'days' => $request->days[$i],
        //         'type' => $request->type[$i],
        //         'fine_id' => $fine->id,
        //     ];
        //     $fine_system = Fine_System::create($data2);
        // }

        // Session::flash('flash_success', 'Fine created successfully!.');
        // Session::flash('flash_type', 'alert-success');
        // return redirect()->route('admin.fines.index');

    }

    public function edit(Fee_Student $payment)
    {
        abort_if(Gate::denies('payment-edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        // $faculties = Faculty::all()->pluck('name','id');
        $groups = Auth::user()->groups;
        $payment->load('fee','student');
        $faculty_id = $payment->fee->batch->course->faculty->id;
        $fines = Fine::where('faculty_id',$faculty_id)->with(['fine_systems'=> function($query)
        {
            $query->orderBy('days');
        }])->get();

        $payment_date = Carbon::now();
        $due = $payment->fee->due_date;
        $due_date = Carbon::parse($due);
        
        $diff_days='';
        if($payment_date > $due_date) {

            $diff_days = $payment_date->diffInDays($due_date);
        }

        // dd($diff_days);
        
        return view('admin.backend.payments.edit', compact('payment','groups','fines','diff_days'));
    }

    public function update(Request $request, Fee_Student $payment)
    {
        // dd($request->all());
        $due=$payment->due_amount - $request->fees;
        $data=[
            'due_amount' => $due,
            'payment_status' => ($due==0)?1:0,
            'fee_status' => ($due==0)?1:0,
        ];
        // dd($data);
        $payment->update($data);

        $data2=[
            'payment_no' => $request->payment_no,
            'slug' => str_slug($request->payment_no),
            'fee_id' => $payment->fee->id,
            'student_id' => $payment->student->id,
            'description' => $request->description,
            'payment_amount' => $request->payment_amount,
            'payment_date' => $request->payment_date,
            'created_by' => Auth::user()->name,
        ];
        $latest = Payment::create($data2);

        for($i=0;$i<count($request->fine_title);$i++){
            $data3=[
                'fine_title' => $request->fine_title[$i],
                'unit' => $request->unit[$i],
                'price' => $request->price[$i],
                'amount' => $request->fine[$i],
                'payment_id' => $latest->id,
            ];
            $fine_payment = Fine_Payment::create($data3);
        }

        Session::flash('flash_success', 'Payments made successfully!.');
        Session::flash('flash_type', 'alert-success');
        return redirect()->route('admin.payments.index');

    }

    public function show(Fine $fine)
    {
        // abort_if(Gate::denies('fine-show'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        // $fine->load('faculty');
        // $fine_system = Fine_System::where('fine_id',$fine->id)->get();
        // return view('admin.backend.fines.show', compact('fine','fine_system'));
    }

    public function destroy(Fine $fine)
    {
        // abort_if(Gate::denies('fine-delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        // $fine->delete();

        // Session::flash('flash_danger', 'fine has been deleted !.');
        // Session::flash('flash_type', 'alert-danger');
        // return back();

    }

    public function massDestroy(MassDestroyFineRequest $request)
    {
        // Fine::whereIn('id', request('ids'))->delete();

        // return response(null, Response::HTTP_NO_CONTENT);

    }

    public function allReceipts(){
        abort_if(Gate::denies('payment-access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $user = Auth::user();
        $groups = $user->groups()->pluck('title');
        $receipts = [];
       
        //getting only accessible fees for user group
            foreach($groups as $key => $group){
                if($group == 'Owner'){
                    $receipts = Payment::with('fee','student')->get();
                    break;
                }
                else{
                    $receipt_under_faculty[$key] = Payment::whereHas('fee', function ($query) use ($group) {
                                                            $query->whereHas('batch', function($query) use ($group) {
                                                                $query->whereHas('course', function($query) use ($group) {
                                                                    $query->whereHas('faculty', function($query) use ($group) {
                                                                        $query->where('name', $group);
                                                                    });
                                                                });
                                                            });
                                                        })->get();
                }
            }
        // converting multidimentional array of batches to single in order to return to view
            if(isset($receipt_under_faculty) && !empty($receipt_under_faculty)){
                foreach($receipt_under_faculty as $receipt){
                    foreach($receipt as $r){
                        $receipts[] = $r;
                    }
                }
            }
                        // dd($receipts);

                
        return view('admin.backend.receipts.index', compact('receipts'));
    }

    public function generateReceipt(Payment $receipt)
    {
        abort_if(Gate::denies('payment-edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');
      
        $receipt->load('fee','student','fine_payments');
       
        return view('admin.backend.receipts.edit', compact('receipt'));
    }

    public function printReceipt(Payment $receipt)
    {
        abort_if(Gate::denies('payment-edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');
     
        $receipt->load('fee','student','fine_payments');
       
        return view('admin.backend.receipts.receipt-print', compact('receipt'));
    }
}
