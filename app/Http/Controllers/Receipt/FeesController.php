<?php

namespace App\Http\Controllers\Receipt;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Fee;
use App\Models\Batch;
use App\Models\Faculty;
use App\Models\Program;
use App\Models\Course;
use App\Models\ProgramType;
use App\Models\Student;
use Gate;
use Symfony\Component\HttpFoundation\Response;
use App\Http\Requests\Receipt\StoreFeeRequest;
use App\Http\Requests\Receipt\UpdateFeeRequest;
use App\Http\Requests\Receipt\MassDestroyFeeRequest;
use Session;
use Auth;

class FeesController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    //
    public function index()
    {
       
        abort_if(Gate::denies('fee-access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $user = Auth::user();
        $roles = $user->roles()->pluck('title');        
        $groups = $user->groups()->pluck('title');


        /*
            $fees =  $roles->map(function ($role) use ($user) {
                    if(strtolower($role) == 'admin' || 'superadmin') {
                        return Fee::with('batch')->get();
                    }
                    return Fee::with('batch')->where('created_by',$user->name)->get();
                });
        */

        $fees = [];
        // foreach($roles as $role) {
            
        //     if(strtolower($role) == 'super admin' || strtolower($role) == 'admin') {
        //         $fees =  Fee::with('batch')->get();
        //     }
        //     else{
        //         $fees =  Fee::with('batch')->where('created_by',$user->name)->get();
        //     }
        // }
    
        //getting only accessible courses for user group
            foreach($groups as $key => $group){
                if($group == 'Owner'){
                    $fees = Fee::with('batch')->get();
                    break;
                }
                else{
                    $fees_under_faculty[$key] = Fee::whereHas('batch', function ($query) use ($group) {
                                                            $query->whereHas('course', function($query) use ($group) {
                                                                $query->whereHas('faculty', function($query) use ($group) {
                                                                    $query->where('name', $group);
                                                                });
                                                            });
                                                        })->get();
                }
            }
        //converting multidimentional array of batches to single in order to return to view
            if(isset($fees_under_faculty) && !empty($fees_under_faculty)){
                foreach($fees_under_faculty as $feees){
                    foreach($feees as $fee){
                        $fees[] = $fee;
                    }
                }
            }
                // dd($fees);
        return view('admin.backend.fees.index', compact('fees'));
    }

    public function create()
    {
        abort_if(Gate::denies('fee-create'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $faculties = Faculty::pluck('name','id');
        // $programs = Program::pluck('name','id');
        $groups = Auth::user()->groups;
        return view('admin.backend.fees.create',compact('faculties','groups'));
    }

    public function store(StoreFeeRequest $request)
    {
        // dd($request->all());
        $data=[
            'fee_no' => $request->fee_no,
            'slug' => str_slug($request->fee_no),
            'type' => $request->type,
            'trimester' => $request->sem,
            'amount' => $request->amount,
            'description' => $request->description,
            'billing_date' => $request->billing_date, 
            'due_date' => $request->due_date,
            'created_by' => Auth::user()->name,
            'batch_id' => $request->batch_id
        ];
        $max_weight = Fee::pluck('weight')->max();

            if($request['weight'] == null){

                $data['weight'] = $max_weight + 10;
            }
        // dd($data);
        $fee = Fee::create($data);
        
        $students = Student::where('batch_id',$request->batch_id)->pluck('id');

        $students->map(function($student) use ($fee) {
            $fee->students()->sync([ $student => ['due_amount' => $fee->amount] ]);
        });

        Session::flash('flash_success', 'Fees created successfully!.');
        Session::flash('flash_type', 'alert-success');
        return redirect()->route('admin.fees.index');

    }

    public function edit(Fee $fee)
    {
        abort_if(Gate::denies('fee-edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $faculties = Faculty::all()->pluck('name','id');
        $groups = Auth::user()->groups;
        $fee->load('batch');
        $course = $fee->batch->course;
        $program = $course->program;
        $faculty = $course->faculty;
        // dd($course->name);
        // $program_id = $fee->course->program_id;
        // dd($program_id);
        $programs = Program::whereHas('faculties' , function ($query) use ($faculty)
                                {
                                    $query->where('faculty_id', $faculty->id);
                                })
                                ->pluck('name','id');
        // dd($fee);
        $courses = Course::where([
                                    ['faculty_id',$faculty->id],
                                    ['program_id',$program->id]
                                ])->pluck('name','id');
        
        return view('admin.backend.fees.edit', compact('fee','faculties','groups','program','course','faculty','programs','courses'));
    }

    public function update(UpdateFeeRequest $request, Fee $fee)
    {
        // dd($request);
        $data=[
            'fee_no' => $request->fee_no,
            'slug' => str_slug($request->fee_no),
            'type' => $request->type,
            'trimester' => $request->sem,
            'amount' => $request->amount,
            'description' => $request->description,
            'billing_date' => $request->billing_date, 
            'due_date' => $request->due_date,
            'updated_by' => Auth::user()->name,
            'batch_id' => $request->batch_id
        ];
        // dd($data);
        $fee->update($data);

        $students = Student::where('batch_id',$request->batch_id)->pluck('id');

        $students->map(function($student) use ($fee) {
            $fee->students()->sync([ $student => ['due_amount' => $fee->amount] ]);
        });

        Session::flash('flash_success', 'Fees updated successfully!.');
        Session::flash('flash_type', 'alert-success');
        return redirect()->route('admin.fees.index');

    }

    public function show(Fee $fee)
    {
        abort_if(Gate::denies('fee-show'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $fee->load('batch');
        return view('admin.backend.fees.show', compact('fee'));
    }

    public function destroy(Fee $fee)
    {
        abort_if(Gate::denies('fee-delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $fee->delete();

        Session::flash('flash_danger', 'fee has been deleted !.');
        Session::flash('flash_type', 'alert-danger');
        return back();

    }

    public function massDestroy(MassDestroyFeeRequest $request)
    {
        Fee::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);

    }

    public function divideTypes(Request $request){
        $course_id = $request->courseId;
        $type_id = $request->typeId;
        
        $course = Course::find($course_id);
        $type = ProgramType::find($type_id);

        return json_encode(array($course, $type));

    }
}
