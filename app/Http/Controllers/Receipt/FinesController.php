<?php

namespace App\Http\Controllers\Receipt;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Fee;
use App\Models\Fine;
use App\Models\Faculty;
use App\Models\Fine_System;
// use App\Models\Course;
// use App\Models\ProgramType;
// use App\Models\Student;
use Gate;
use Symfony\Component\HttpFoundation\Response;
use App\Http\Requests\Receipt\StoreFineRequest;
use App\Http\Requests\Receipt\UpdateFineRequest;
use App\Http\Requests\Receipt\MassDestroyFineRequest;
use Session;
use Auth;

class FinesController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    //
    public function index()
    {
       
        abort_if(Gate::denies('fine-access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $user = Auth::user();
        // // $roles = $user->roles()->pluck('title');
        $groups = $user->groups()->pluck('title');

        $fines = [];
        // foreach($roles as $role) {
            
        //     if(strtolower($role) == 'super admin' || strtolower($role) == 'admin') {
        //         $fines =  Fine::with('faculty')->get();
        //     }
        //     else{
        //         $fines =  Fine::with('faculty')->where('created_by',$user->name)->get();
        //     }
        // }
    
        //getting only accessible fines for user group
            foreach($groups as $key => $group){
                if($group == 'Owner'){
                    $fines = Fine::with('faculty')->get();
                    break;
                }
                else{
                    $fines_under_faculty[$key] = Fine::whereHas('faculty', function ($query) use ($group) {
                                                            $query->where('name', $group);
                                                        })->get();
                }
            }
        //converting multidimentional array of fines to single in order to return to view
            if(isset($fines_under_faculty) && !empty($fines_under_faculty)){
                foreach($fines_under_faculty as $acccessfines){
                    foreach($acccessfines as $fine){
                        $fines[] = $fine;
                    }
                }
            }
                // dd($batches);
        return view('admin.backend.fines.index', compact('fines'));
    }

    public function create()
    {
        abort_if(Gate::denies('fine-create'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $faculties = Faculty::pluck('name','id');
        $groups = Auth::user()->groups;
        return view('admin.backend.fines.create',compact('faculties','groups'));
    }

    public function store(StoreFineRequest $request)
    {
        // dd($request->all());
        $data=[
            'title' => $request->title,
            'slug' => str_slug($request->title),
            'code' => $request->code,
            'faculty_id' => $request->faculty_id, 
            'status' => $request->status,
            'description' => $request->description,
            'created_by' => Auth::user()->name,
        ];
        $fine = Fine::create($data);

        for($i=0;$i<count($request->amount);$i++){
            $data2=[
                'amount' => $request->amount[$i],
                'days' => $request->days[$i],
                'type' => $request->type[$i],
                'fine_id' => $fine->id,
            ];
            $fine_system = Fine_System::create($data2);
        }

        Session::flash('flash_success', 'Fine created successfully!.');
        Session::flash('flash_type', 'alert-success');
        return redirect()->route('admin.fines.index');

    }

    public function edit(Fine $fine)
    {
        abort_if(Gate::denies('fine-edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $faculties = Faculty::all()->pluck('name','id');
        $groups = Auth::user()->groups;
        $fine->load('faculty');
        $fine_system = Fine_System::where('fine_id',$fine->id)->get();
        
        return view('admin.backend.fines.edit', compact('fine','faculties','groups','fine_system'));
    }

    public function update(UpdateFineRequest $request, Fine $fine)
    {
        $data=[
            'title' => $request->title,
            'slug' => str_slug($request->title),
            'code' => $request->code,
            'faculty_id' => $request->faculty_id, 
            'status' => $request->status,
            'description' => $request->description,
            'created_by' => Auth::user()->name,
        ];
        // dd($data);
        $fine->update($data);
        Fine_System::where('fine_id',$fine->id)->delete();
        for($i=0;$i<count($request->amount);$i++){
            $data2=[
                'amount' => $request->amount[$i],
                'days' => $request->days[$i],
                'type' => $request->type[$i],
                'fine_id' => $fine->id,
            ];
            $fine_system = Fine_System::create($data2);
        }

        Session::flash('flash_success', 'fines updated successfully!.');
        Session::flash('flash_type', 'alert-success');
        return redirect()->route('admin.fines.index');

    }

    public function show(Fine $fine)
    {
        abort_if(Gate::denies('fine-show'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $fine->load('faculty');
        $fine_system = Fine_System::where('fine_id',$fine->id)->get();
        return view('admin.backend.fines.show', compact('fine','fine_system'));
    }

    public function destroy(Fine $fine)
    {
        abort_if(Gate::denies('fine-delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $fine->delete();

        Session::flash('flash_danger', 'fine has been deleted !.');
        Session::flash('flash_type', 'alert-danger');
        return back();

    }

    public function massDestroy(MassDestroyFineRequest $request)
    {
        Fine::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);

    }

}
