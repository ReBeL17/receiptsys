<?php

namespace App\Http\Controllers\Receipt;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Batch;
use App\Models\Faculty;
use App\Models\Program;
use App\Models\Course;
use Gate;
use Symfony\Component\HttpFoundation\Response;
use App\Http\Requests\Receipt\StoreBatchRequest;
use App\Http\Requests\Receipt\UpdateBatchRequest;
use App\Http\Requests\Receipt\MassDestroyBatchRequest;
use Session;
use Auth;

class BatchesController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    //
    public function index()
    {
       
        abort_if(Gate::denies('batch-access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $groups = Auth::user()->groups()->pluck('title');
        $batches = [];
        
        //getting only accessible courses for user group
        foreach($groups as $key => $group){
            if($group == 'Owner'){
                $batches = Batch::all();
                break;
            }
            else{
                $batches_under_faculty[$key] = Batch::whereHas('course', function ($query) use ($group) {
                                                        $query->whereHas('faculty', function($q) use ($group) {
                                                            $q->where('name', $group);
                                                        });
                                                    })->get();
            }
        }
        //converting multidimentional array of batches to single in order to return to view
        if(isset($batches_under_faculty) && !empty($batches_under_faculty)){
            foreach($batches_under_faculty as $batch){
                foreach($batch as $b){
                    $batches[] = $b;
                }
            }
        }
                // dd($batches);
        return view('admin.backend.batches.index', compact('batches'));
    }

    public function create()
    {
        abort_if(Gate::denies('batch-create'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $faculties = Faculty::all()->pluck('name','id');
        $groups = Auth::user()->groups;
        return view('admin.backend.batches.create',compact('faculties','groups'));
    }

    public function store(StoreBatchRequest $request)
    {
        // dd($request->all());
        $data=[
            'name' => $request->name,
            'description' => $request->description,
            'slug' => str_slug($request->name),
            'course_id' => $request->course_id,
            'coursetype_id' => $request->coursetypes
        ];
        // dd($data);
        $batch = Batch::create($data);

        Session::flash('flash_success', 'batch created successfully!.');
        Session::flash('flash_type', 'alert-success');
        return redirect()->route('admin.batches.index');

    }

    public function edit(Batch $batch)
    {
        abort_if(Gate::denies('batch-edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $faculties = Faculty::all()->pluck('name','id');
        $groups = Auth::user()->groups;
        $batch->load('course');
        $faculty_id = $batch->faculty->id;
        $program_id = $batch->program->id;
        // dd($faculty_id);
        $programs = Program::whereHas('faculties' , function ($query) use ($faculty_id)
                                {
                                    $query->where('faculty_id', $faculty_id);
                                })
                                ->with('faculties')
                                ->pluck('name','id');
        // dd($batch);
        $courses = Course::where([
                                    ['faculty_id',$faculty_id],
                                    ['program_id',$program_id]
                                ])->pluck('name','id');
        // dd($courses);
        return view('admin.backend.batches.edit', compact('batch','faculties','groups','programs','courses'));
    }

    public function update(UpdateBatchRequest $request, Batch $batch)
    {
        // dd($request);
        $data=[
            'name' => $request->name,
            'description' => $request->description,
            'slug' => str_slug($request->name),
            'course_id' => $request->course_id,
            'coursetype_id' => $request->coursetypes
        ];
        // dd($data);
        $batch->update($data);

        Session::flash('flash_success', 'batch updated successfully!.');
        Session::flash('flash_type', 'alert-success');
        return redirect()->route('admin.batches.index');

    }

    public function show(Batch $batch)
    {
        abort_if(Gate::denies('batch-show'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $batch->load('course','coursetype');
        return view('admin.backend.batches.show', compact('batch'));
    }

    public function destroy(Batch $batch)
    {
        abort_if(Gate::denies('batch-delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $batch->delete();

        Session::flash('flash_danger', 'batch has been deleted !.');
        Session::flash('flash_type', 'alert-danger');
        return back();

    }

    public function massDestroy(MassDestroyBatchRequest $request)
    {
        Batch::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);

    }

    public function getBatches(Request $request)
    {
        $course_id = $request->courseId;

        //getting two different type of values (batches and course) from one method
        $batches = Batch::where('course_id',$course_id)->get();
        
        //to get course types
        $course = Course::with('coursetypes')->find($course_id);

        // encoding two values to return to ajax
        return json_encode(array($batches,$course));
    }
}
