<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Gate;
use Symfony\Component\HttpFoundation\Response;
use App\Admin;
use App\Role;
use App\Group;
use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Http\Requests\MassDestroyUserRequest;
use Session;
use Auth;
// use Illuminate\Support\Collection;

class UsersController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    //
    public function index()
    {
       
        abort_if(Gate::denies('user-access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $roles = Auth::user()->roles()->pluck('title');
        // dd($roles);
        //getting only accessible courses for user group
        foreach($roles as $key => $role){
            if($role == 'Super Admin'){
                $users = Admin::with('roles')->get();
                break;
            }
            elseif($role == 'Admin'){
                $users = Admin::whereHas('roles', function ($query) use ($role) {
                    $query->where('name','!=','Super Admin');
                    })->get();
            }
        }
        // $users = Admin::with('roles')->get();
        
        // dd($users);
        return view('admin.backend.users.index', compact('users'));
    }

    public function create()
    {
        abort_if(Gate::denies('user-create'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $all_roles = Role::pluck('title', 'id');
        // $roles = Auth::user()->roles;
        $roles = $all_roles->filter(function($role) {
                return $role != 'Super Admin';
        });
        $groups = Group::all()->pluck('title', 'id');
        return view('admin.backend.users.create', compact('roles','groups'));
    }

    public function store(StoreUserRequest $request)
    {
        // dd($request->all());
        $data=[
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password)
        ];
        // dd($data);
        $user = Admin::create($data);
        $user->roles()->sync($request->input('roles', []));
        $user->groups()->sync($request->input('groups', []));

        Session::flash('flash_success', 'User created successfully!.');
        Session::flash('flash_type', 'alert-success');
        return redirect()->route('admin.users.index');

    }

    public function edit(Admin $user)
    {
        abort_if(Gate::denies('user-edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $user->load(['roles','groups']);
        
        $all_roles = Role::all()->pluck('title', 'id');
        $roles = $all_roles->filter(function($role) {
            return $role != 'Super Admin';
        });
        $groups = Group::all()->pluck('title', 'id');

        return view('admin.backend.users.edit', compact('user','roles','groups'));
    }

    public function update(UpdateUserRequest $request, Admin $user)
    {
        
        $data=[
            'name' => $request->name,
            'email' => $request->email,

        ];
        
        if($request->password !== $user->password){

            $data['password'] = bcrypt($request->password);
        }
        // dd($data);
        $user->update($data);
        $user->roles()->sync($request->input('roles', []));
        $user->groups()->sync($request->input('groups', []));
        Session::flash('flash_success', 'User updated successfully!.');
        Session::flash('flash_type', 'alert-success');
        return redirect()->route('admin.users.index');

    }

    public function show(Admin $user)
    {
        abort_if(Gate::denies('user-show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.backend.users.show', compact('user'));
    }

    public function destroy(Admin $user)
    {
        abort_if(Gate::denies('user-delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $user->delete();

        Session::flash('flash_danger', 'User has been deleted !.');
        Session::flash('flash_type', 'alert-danger');
        return back();

    }

    public function massDestroy(MassDestroyUserRequest $request)
    {
        // dd($request);
        Admin::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);

    }
}
