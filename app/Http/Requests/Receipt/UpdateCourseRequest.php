<?php

namespace App\Http\Requests\Receipt;

use App\Models\Course;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class UpdateCourseRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('course-edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;

    }

    public function rules()
    {
        return [
            'name' => [
                'required',
                'unique:courses,name,' . request()->route('course')->id],
            'duration' => [
                'required'],
            'programtypes' => [
                'required'],
            'faculty_id' => [
                'required'],
            'program_id' => [
                'required'],
        ];

    }
}