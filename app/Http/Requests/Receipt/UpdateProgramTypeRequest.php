<?php

namespace App\Http\Requests\Receipt;

use App\Models\ProgramType;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class UpdateProgramTypeRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('programtype-edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;

    }

    public function rules()
    {
        return [
            'name' => [
                'required',
                'unique:programTypes,name,' . request()->route('programtype')->id],
        ];

    }
}