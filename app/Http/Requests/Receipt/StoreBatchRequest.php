<?php

namespace App\Http\Requests\Receipt;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Batch;
use Gate;
use Symfony\Component\HttpFoundation\Response;

class StoreBatchRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        abort_if(Gate::denies('batch-create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
              //
            'name' => [
                'required'],
            'faculty_id' => [
                'required'],
            'program_id' => [
                'required'],
            'course_id' => [
                'required'],
            'coursetypes' => [
                'required'],
        ];
    }
}
