<?php

namespace App\Http\Requests\Receipt;

use App\Models\Fine;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class MassDestroyFineRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('fine-delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;

    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:fines,id',
        ];

    }
}