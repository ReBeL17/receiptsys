<?php

namespace App\Http\Requests\Receipt;

use App\Models\Fine;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class UpdateFineRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('fine-edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;

    }

    public function rules()
    {
        return [
            'faculty_id' => [
                'required'],
            'title' => [
                'required'],
            'code' => [
                'required',
                'unique:fines,code,' . request()->route('fine')->id],
            'days' => [
                'required'],
            'amount' => [
                'required'],
            'type' => [
                'required'],
            'status' => [
                'required'],
        ];

    }
}