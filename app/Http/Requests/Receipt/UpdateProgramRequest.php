<?php

namespace App\Http\Requests\Receipt;

use App\Models\Program;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class UpdateProgramRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('program-edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;

    }

    public function rules()
    {
        return [
            'name' => [
                'required',
                'unique:programs,name,' . request()->route('program')->id],
        ];

    }
}