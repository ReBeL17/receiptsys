<?php

namespace App\Http\Requests\Receipt;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Fee;
use Gate;
use Symfony\Component\HttpFoundation\Response;

class StoreFeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        abort_if(Gate::denies('fee-create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'fee_no' => [
                'required',
                'unique:fees'],
            'billing_date' => [
                'required'],
            'faculty_id' => [
                'required'],
            'program_id' => [
                'required'],
            'course_id' => [
                'required'],
            'batch_id' => [
                'required'],
            'type' => [
                'required'],
            'sem' => [
                'required'],
            'amount' => [
                'required'],
        ];
    }
}
