<?php

namespace App\Http\Requests\Receipt;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Student;
use Gate;
use Symfony\Component\HttpFoundation\Response;

class StoreStudentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        abort_if(Gate::denies('student-create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'regd_no' => [
                'required',
                'unique:students'],
            'name' => [
                'required'],
            'faculty_id' => [
                'required'],
            'program_id' => [
                'required'],
            'course_id' => [
                'required'],
            'batch_id' => [
                'required'],
            'address' => [
                'required'],
            'contact' => [
                'required'],
        ];
    }
}
