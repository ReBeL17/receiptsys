<?php

namespace App\Http\Requests\Receipt;

use App\Models\Fee;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class UpdateFeeRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('fee-edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;

    }

    public function rules()
    {
        return [
            'fee_no' => [
                'required',
                'unique:fees,fee_no,' . request()->route('fee')->id],
            'billing_date' => [
                'required'],
            'faculty_id' => [
                'required'],
            'program_id' => [
                'required'],
            'course_id' => [
                'required'],
            'batch_id' => [
                'required'],
            'type' => [
                'required'],
            'sem' => [
                'required'],
            'amount' => [
                'required'],
        ];

    }
}