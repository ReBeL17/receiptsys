<?php

namespace App\Http\Requests\Receipt;

use App\Models\Student;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class UpdateStudentRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('student-edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;

    }

    public function rules()
    {
        return [
            'regd_no' => [
                'required',
                'unique:students,regd_no,' . request()->route('student')->id],
            'name' => [
                'required'],
            'faculty_id' => [
                'required'],
            'program_id' => [
                'required'],
            'course_id' => [
                'required'],
            'batch_id' => [
                'required'],
            'address' => [
                'required'],
            'contact' => [
                'required'],
        ];

    }
}