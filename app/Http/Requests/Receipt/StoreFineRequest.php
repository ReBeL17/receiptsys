<?php

namespace App\Http\Requests\Receipt;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Fine;
use Gate;
use Symfony\Component\HttpFoundation\Response;

class StoreFineRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        abort_if(Gate::denies('fine-create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
         
            'faculty_id' => [
                'required'],
            'title' => [
                'required'],
            'code' => [
                'required',
                'unique:fines'],
            'days' => [
                'required'],
            'amount' => [
                'required'],
            'type' => [
                'required'],
            'status' => [
                'required'],
        ];
    }
}
