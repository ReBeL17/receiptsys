<?php

namespace App\Http\Requests\Receipt;

use App\Models\Batch;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class UpdateBatchRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('batch-edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;

    }

    public function rules()
    {
        return [
            'name' => [
                'required'],
            'course_id' => [
                'required'],
            'faculty_id' => [
                'required'],
            'program_id' => [
                'required'],
            'coursetypes' => [
                'required'],
        ];

    }
}