<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProgramType extends Model
{
    //
    use SoftDeletes;

    protected $table = 'programtypes';

    protected $dates = [
      'created_at',
      'updated_at',
      'deleted_at',
  ];
  
    protected $fillable = [
      'name',
      'slug',
      'description',
      'created_at',
      'updated_at',
      'deleted_at',
  ];

     /**
         * Override parent boot and Call deleting event
         *
         * @return void
         */
        protected static function boot() 
        {
            parent::boot();

            static::deleting(function($coursetypes) {
                foreach ($coursetypes->batches()->get() as $batch) {
                $batch->delete();
                }
            });
        }

    public function batches() {

        return $this->hasMany(Batch::class,'coursetype_id');
            
     }
     
     public function courses() {
     
        return $this->belongsToMany(Course::class,'courses_coursetypes','coursetype_id','course_id');
            
     }

     public function getRouteKeyName() {
       
          return 'slug';
      }
}
