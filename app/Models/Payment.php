<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Payment extends Model
{
    //
    use SoftDeletes;

    protected $dates = [
      'created_at',
      'updated_at',
      'deleted_at',
  ];
  
    protected $fillable = [
      'fee_id',
      'slug',
      'description',
      'payment_no',
      'student_id',
      'payment_amount',
      'payment_date',
      'created_by',
      'updated_by',
      'deleted_by',
      'created_at',
      'updated_at',
      'deleted_at',
  ];
      
      public function fee() {
    
          return $this->belongsTo(Fee::class);
              
       }

       public function student() {
    
        return $this->belongsTo(Student::class);
            
     }

     public function fine_payments() {
    
      return $this->hasMany(Fine_Payment::class);
          
   }

    //   public function students(){
    //     return $this->belongsToMany(Student::class)->withPivot('payment_status', 'fee_status', 'due_amount')->withTimestamps();
    //   }
   
    
    //   protected $appends = ['faculty','program']; // only available in Laravel 5
    
    //   public function getFacultyAttribute() {

    //       return $this->course->faculty;

    //   }

    //   public function getProgramAttribute() {

    //       return $this->course->program;

    //   }

     public function getRouteKeyName() {
       
          return 'slug';
      }
}
