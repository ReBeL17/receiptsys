<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Fine extends Model
{
    //
    use SoftDeletes;

    protected $dates = [
      'created_at',
      'updated_at',
      'deleted_at',
  ];
  
    protected $fillable = [
      'title',
      'slug',
      'description',
      'code',
      'status',
      'type',
      'days',
      'amount',
      'faculty_id',
      'created_by',
      'updated_by',
      'deleted_by',
      'created_at',
      'updated_at',
      'deleted_at',
  ];
      
      public function faculty() {
    
          return $this->belongsTo(Faculty::class);
              
       }

      public function fine_systems(){
        return $this->hasMany(Fine_System::class);
      }
   
    
    //   protected $appends = ['faculty','program']; // only available in Laravel 5
    
    //   public function getFacultyAttribute() {

    //       return $this->course->faculty;

    //   }

    //   public function getProgramAttribute() {

    //       return $this->course->program;

    //   }

     public function getRouteKeyName() {
       
          return 'slug';
      }
}
