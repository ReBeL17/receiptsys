<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Fee_Student extends Model
{
    //
    protected $table = 'fee_student';

    protected $fillable = [
        'payment_status',
        'fee_status',
        'due_amount',
        'created_at',
        'updated_at',
    ];

    public function fee(){
        return $this->belongsTo(Fee::class);
    }

    public function student(){
        return $this->belongsTo(Student::class);
    }
}
