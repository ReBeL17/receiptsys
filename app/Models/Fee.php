<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Fee extends Model
{
    //
    use SoftDeletes;

    protected $dates = [
      'created_at',
      'updated_at',
      'deleted_at',
  ];
  
    protected $fillable = [
      'fee_no',
      'slug',
      'description',
      'trimester',
      'amount',
      'batch_id',
      'weight',
      'billing_date',
      'due_date',
      'created_by',
      'updated_by',
      'deleted_by',
      'created_at',
      'updated_at',
      'deleted_at',
  ];
      
      public function batch() {
    
          return $this->belongsTo(Batch::class);
              
       }

      public function students(){
        return $this->belongsToMany(Student::class)->withPivot('payment_status', 'fee_status', 'due_amount')->withTimestamps();
      }
   
      public function payments(){
        return $this->hasMany(Payment::class);
      }

    //   protected $appends = ['faculty','program']; // only available in Laravel 5
    
    //   public function getFacultyAttribute() {

    //       return $this->course->faculty;

    //   }

    //   public function getProgramAttribute() {

    //       return $this->course->program;

    //   }

     public function getRouteKeyName() {
       
          return 'slug';
      }
}
