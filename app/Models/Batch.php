<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Batch extends Model
{
    //
    use SoftDeletes;

    protected $dates = [
      'created_at',
      'updated_at',
      'deleted_at',
  ];
  
    protected $fillable = [
      'name',
      'slug',
      'description',
      'course_id',
      'coursetype_id',
      'created_at',
      'updated_at',
      'deleted_at',
  ];
      
      public function course() {
    
          return $this->belongsTo(Course::class);
              
       }

   
      protected $appends = ['faculty','program']; // only available in Laravel 5
    
      public function getFacultyAttribute() {

          return $this->course->faculty;

      }

      public function getProgramAttribute() {

          return $this->course->program;

      }

            /**
         * Override parent boot and Call deleting event
         *
         * @return void
         */
        protected static function boot() 
        {
            parent::boot();

            static::deleting(function($batches) {
                foreach ($batches->fees()->get() as $fee) {
                $fee->delete();
                }

                foreach ($batches->students()->get() as $student) {
                    $student->delete();
                    }
            });
        }

      public function fees(){
          return $this->hasMany(Fee::class);
      }

      public function students(){
        return $this->hasMany(Student::class);
    }

      public function coursetype(){
        return $this->belongsTo(ProgramType::class,'coursetype_id');
    }

     public function getRouteKeyName() {
       
          return 'slug';
      }
}
