<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Course extends Model
{
    //
    use SoftDeletes;

    protected $dates = [
      'created_at',
      'updated_at',
      'deleted_at',
  ];
  
    protected $fillable = [
      'name',
      'slug',
      'abbreviation',
      'duration',
      'description',
      'weight',
      'faculty_id',
      'program_id',
      'created_at',
      'updated_at',
      'deleted_at',
      'created_by',
      'updated_by',
      'deleted_by',
  ];
     
     public function faculty() {
     
        return $this->belongsTo(Faculty::class);
            
     }

     public function program() {
     
        return $this->belongsTo(Program::class);
          
      }

      public function coursetypes() {
     
        return $this->belongsToMany(ProgramType::class,'courses_coursetypes','course_id','coursetype_id');
            
     }

         /**
         * Override parent boot and Call deleting event
         *
         * @return void
         */
        protected static function boot() 
        {
            parent::boot();

            static::deleting(function($courses) {
                foreach ($courses->batches()->get() as $batch) {
                $batch->delete();
                }
            });
        }

      public function batches() {

          return $this->hasMany(Batch::class);
              
      }

     public function getRouteKeyName() {
       
          return 'slug';
      }
}
