<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Fine_Payment extends Model
{
    //
    use SoftDeletes;
    protected $table = 'fine_payments';
    protected $dates = [
      'created_at',
      'updated_at',
      'deleted_at',
  ];
  
    protected $fillable = [
      'payment_id',
      'fine_title',
      'unit',
      'price',
      'amount',
      'created_at',
      'updated_at',
      'deleted_at',
  ];
      
      public function payment() {
    
          return $this->belongsTo(Payment::class);
              
       }

      //  public function student() {
    
      //   return $this->belongsTo(Student::class);
            
    //  }

    //   public function students(){
    //     return $this->belongsToMany(Student::class)->withPivot('payment_status', 'fee_status', 'due_amount')->withTimestamps();
    //   }
   
    
    //   protected $appends = ['faculty','program']; // only available in Laravel 5
    
    //   public function getFacultyAttribute() {

    //       return $this->course->faculty;

    //   }

    //   public function getProgramAttribute() {

    //       return $this->course->program;

    //   }

    //  public function getRouteKeyName() {
       
    //       return 'slug';
    //   }
}
