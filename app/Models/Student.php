<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Student extends Model
{
    //
    use SoftDeletes;

    protected $dates = [
      'created_at',
      'updated_at',
      'deleted_at',
  ];
  
    protected $fillable = [
      'name',
      'slug',
      'batch_id',
      'address',
      'contact',
      'regd_no',
      'created_at',
      'updated_at',
      'deleted_at',
  ];
      
      public function batch() {
    
          return $this->belongsTo(Batch::class);
              
       }

   
      protected $appends = ['faculty','program']; // only available in Laravel 5
    
      public function getFacultyAttribute() {

          return $this->course->faculty;

      }

      public function getProgramAttribute() {

          return $this->course->program;

      }

            /**
         * Override parent boot and Call deleting event
         *
         * @return void
         */
        protected static function boot() 
        {
            parent::boot();

            static::deleting(function($batches) {
                foreach ($batches->fees()->get() as $fee) {
                $fee->delete();
                }
            });
        }

      public function fees(){
          return $this->belongsToMany(Fee::class)->withPivot('payment_status', 'fee_status', 'due_amount')->withTimestamps();
      }

      public function coursetype(){
        return $this->belongsTo(ProgramType::class,'coursetype_id');
    }

        public function payments(){
            return $this->hasMany(Payment::class);
        }

     public function getRouteKeyName() {
       
          return 'slug';
      }
}
