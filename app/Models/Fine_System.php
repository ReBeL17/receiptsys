<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Fine_System extends Model
{
    //
    protected $table = 'fine_system';

    protected $fillable = [
        'type',
        'amount',
        'days',
        'fine_id',
        'created_at',
        'updated_at',
    ];

    public function fine(){
        $this->belongsTo(Fine::class);
    }
}
