<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
// Route::get('/logout', 'Auth\LoginController@logout')->name('logout');

Route::get('/', 'Admin\HomeController@index')->name('dashboard');
Route::group(['prefix' => 'admin', 'as' => 'admin.'], function() {
    Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('login');
    Route::post('/login', 'Auth\AdminLoginController@login')->name('login');
    Route::get('/', 'Admin\HomeController@index')->name('dashboard');
    Route::get('/logout', 'Auth\AdminLoginController@logout')->name('logout');

    //password reset routes
    Route::post('/password/email', 'Auth\AdminForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::get('/password/reset', 'Auth\AdminForgotPasswordController@showLinkRequestForm')->name('password.request');
    Route::post('/password/reset', 'Auth\AdminResetPasswordController@reset');
    Route::get('/password/reset/{token}', 'Auth\AdminResetPasswordController@showResetForm')->name('password.reset');

    // Permissions
    Route::delete('permissions/destroy', 'Admin\PermissionsController@massDestroy')->name('permissions.massDestroy');
    Route::resource('permissions', 'Admin\PermissionsController');

    // Roles
    Route::delete('roles/destroy', 'Admin\RolesController@massDestroy')->name('roles.massDestroy');
    Route::resource('roles', 'Admin\RolesController');

    // Users
    Route::delete('users/destroy', 'Admin\UsersController@massDestroy')->name('users.massDestroy');
    Route::resource('users', 'Admin\UsersController');

    // groups
    Route::delete('groups/destroy', 'Admin\GroupsController@massDestroy')->name('groups.massDestroy');
    Route::resource('groups', 'Admin\GroupsController');

    Route::group(['namespace' => 'Receipt'], function() {
        Route::resource('faculty', 'FacultyController');
        Route::delete('faculties/destroy', 'FacultyController@massDestroy')->name('faculty.massDestroy');

        Route::get('programs/getspecificprograms','ProgramsController@getPrograms')->name('programs.getspecificprograms');
        Route::post('programs/addprogram','ProgramsController@addProgram')->name('programs.addProgram');
        Route::delete('programs/destroy', 'ProgramsController@massDestroy')->name('programs.massDestroy');
        Route::resource('programs', 'ProgramsController');

        Route::post('programtypes/addtype','ProgramTypesController@addType')->name('programtypes.addType');
        Route::delete('programtypes/destroy', 'ProgramTypesController@massDestroy')->name('programtypes.massDestroy');
        Route::resource('programtypes', 'ProgramTypesController');

        Route::get('courses/getspecificcourses','CoursesController@getCourses')->name('courses.getspecificcourses');
        Route::get('courses/getcoursetypes','CoursesController@getCourseTypes')->name('courses.getcoursetypes');
        Route::delete('courses/destroy', 'CoursesController@massDestroy')->name('courses.massDestroy');
        Route::resource('courses', 'CoursesController');

        Route::get('batches/getspecificbatches','BatchesController@getBatches')->name('batches.getspecificbatches');
        Route::delete('batches/destroy', 'BatchesController@massDestroy')->name('batches.massDestroy');
        Route::resource('batches', 'BatchesController');

        Route::get('fees/dividetypes','FeesController@divideTypes')->name('fees.divideTypes');
        Route::delete('fees/destroy', 'FeesController@massDestroy')->name('fees.massDestroy');
        Route::resource('fees', 'FeesController');

        Route::delete('students/destroy', 'StudentsController@massDestroy')->name('students.massDestroy');
        Route::resource('students', 'StudentsController');

        Route::delete('fines/destroy', 'FinesController@massDestroy')->name('fines.massDestroy');
        Route::resource('fines', 'FinesController');

        Route::delete('payments/destroy', 'PaymentsController@massDestroy')->name('payments.massDestroy');
        Route::resource('payments', 'PaymentsController');
        Route::get('receipts', 'PaymentsController@allReceipts')->name('payments.allReceipts');
        Route::get('receipts/{receipt}', 'PaymentsController@generateReceipt')->name('payments.generateReceipt');

        Route::get('receipts/{receipt}/print', 'PaymentsController@printReceipt')->name('receipt-print');
    });
});